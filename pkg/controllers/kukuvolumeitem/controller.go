/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitem

import (
	"fmt"

	"github.com/fsnotify/fsnotify"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"kukuvolume-controller/pkg/utils"
	kerrors "kukuvolume-controller/pkg/utils/errors"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	coreinformers "k8s.io/client-go/informers/core/v1"
	kubeclientset "k8s.io/client-go/kubernetes"
	corelisters "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	common "kukuvolume-controller/pkg/controllers/common"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"
)

// Controller is the controller implementation for V3Deployments.
type Controller struct {
	base.BaseController

	config common.ControllerConfig

	kubeClientset   kubeclientset.Interface
	kumoriClientset kumoriclientset.Interface

	persistentVolumeInformer      coreinformers.PersistentVolumeInformer
	persistentVolumeClaimInformer coreinformers.PersistentVolumeClaimInformer
	kukuVolumeInformer            kumoriinformers.KukuVolumeInformer
	kukuVolumeItemInformer        kumoriinformers.KukuVolumeItemInformer
	configMapInformer             coreinformers.ConfigMapInformer

	persistentVolumesLister      corelisters.PersistentVolumeLister
	persistentVolumeClaimsLister corelisters.PersistentVolumeClaimLister
	kukuVolumesLister            kumorilisters.KukuVolumeLister
	kukuVolumeItemsLister        kumorilisters.KukuVolumeItemLister
	configMapLister              corelisters.ConfigMapLister
}

// NewController returns a new kuku controller
func NewController(
	kubeClientset kubeclientset.Interface,
	kumoriClientset kumoriclientset.Interface,
	persistentVolumeInformer coreinformers.PersistentVolumeInformer,
	persistentVolumeClaimInformer coreinformers.PersistentVolumeClaimInformer,
	kukuVolumeInformer kumoriinformers.KukuVolumeInformer,
	kukuVolumeItemInformer kumoriinformers.KukuVolumeItemInformer,
	configMapInformer coreinformers.ConfigMapInformer,
) (c *Controller) {
	meth := "kukuvolume.NewController"
	log.Infof("%s. Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 5)
	syncedFunctions[0] = persistentVolumeInformer.Informer().HasSynced
	syncedFunctions[1] = persistentVolumeClaimInformer.Informer().HasSynced
	syncedFunctions[2] = kukuVolumeInformer.Informer().HasSynced
	syncedFunctions[3] = kukuVolumeItemInformer.Informer().HasSynced
	syncedFunctions[4] = configMapInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = kukuVolumeItemInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 4)
	secondarySharedInformers[0] = persistentVolumeInformer.Informer()
	secondarySharedInformers[1] = persistentVolumeClaimInformer.Informer()
	secondarySharedInformers[2] = kukuVolumeInformer.Informer()
	secondarySharedInformers[3] = configMapInformer.Informer()

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     controllerKind,
		Workqueue:                workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), controllerName),
		Recorder:                 base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName),
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}

	configMapLister := configMapInformer.Lister()
	config := common.GetConfig(configMapLister)

	c = &Controller{
		BaseController:                baseController,
		config:                        config,
		kubeClientset:                 kubeClientset,
		kumoriClientset:               kumoriClientset,
		persistentVolumeInformer:      persistentVolumeInformer,
		persistentVolumeClaimInformer: persistentVolumeClaimInformer,
		kukuVolumeInformer:            kukuVolumeInformer,
		kukuVolumeItemInformer:        kukuVolumeItemInformer,
		configMapInformer:             configMapInformer,
		persistentVolumesLister:       persistentVolumeInformer.Lister(),
		persistentVolumeClaimsLister:  persistentVolumeClaimInformer.Lister(),
		kukuVolumesLister:             kukuVolumeInformer.Lister(),
		kukuVolumeItemsLister:         kukuVolumeItemInformer.Lister(),
		configMapLister:               configMapLister,
	}

	c.BaseController.IBaseController = c

	// Watches for changes in configuration. If a change is detected, all KukuVolumeItem objects
	// are queued.
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Infof("%s. Config file changed: %s", meth, e.Name)
		config := common.GetConfig(configMapLister)
		c.config = config
		UpdateLogLevel()
		c.enqueueAllVolumeItems()
	})
	return
}

// HandleSecondary is in charge of processing changes in the secondary objects
// and force to re-evaluate the primary object
func (c *Controller) HandleSecondary(secondary interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Info(meth)
	obj, kukuVolumeitemName, err := c.GetObject(secondary)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}

	// If the object belongs to a kukuVolumeItem, enqueues the kukuVolumeItem
	if kukuVolumeitemName != "" {
		log.Debugf("%s Processing object: %s", meth, obj.GetName())
		kukuVolumeItem, err := c.kukuVolumeItemsLister.KukuVolumeItems(obj.GetNamespace()).Get(kukuVolumeitemName)
		if errors.IsNotFound(err) {
			log.Infof("%s ignoring orphaned object '%s' of KukuVolumeItem '%s'", meth, obj.GetSelfLink(), kukuVolumeitemName)
			return
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, kukuVolumeitemName, err)
		}
		log.Debugf("%s enqueue %s", meth, kukuVolumeItem.Name)
		c.EnqueueObject(kukuVolumeItem)
		return
	}

	// Otherwise, figures out if the object is related with a KukuVolumeItem
	switch obj := obj.(type) {
	case *corev1.PersistentVolumeClaim:
		c.handlePersistentVolumeClaim(obj)
	case *corev1.PersistentVolume:
		c.handlePersistentVolume(obj)
	case *kumoriv1.KukuVolume:
		c.handleKukuVolume(obj)
	case *corev1.ConfigMap:
		c.handleConfigMap(obj)
	default:
		log.Warnf("%s. Unknown object '%s' type. Skipping", meth, obj.GetName())
	}
}

func (c *Controller) handlePersistentVolumeClaim(pvc *corev1.PersistentVolumeClaim) {

	// Gets the PVC name and namespace
	pvcname := pvc.GetName()
	pvcns := pvc.GetNamespace()
	meth := fmt.Sprintf("%s.handlePersistentVolumeClaim. PersistentVolumeClaim: '%s/%s'", c.Name, pvcns, pvcname)
	log.Debug(meth)

	// Creates the labels filter to find the KukuVolumeItem related with this PVC
	matchLabels := map[string]string{
		utils.PersistentVolumeClaimLabel: pvcname,
	}
	options := labels.SelectorFromSet(matchLabels)

	// Gets the KukuVolumeItems labeled with this PVC
	kukuVolumeItems, err := kviutils.List(c.kukuVolumeItemsLister, c.config.Namespace, options)
	if err != nil && !errors.IsNotFound(err) {
		log.Errorf("%s. Error retrieving KukuVolumeItem: %v", meth, err)
		return
	}

	// Skip if there are not KukuVolumeItems related to this PVC
	if kukuVolumeItems == nil || len(kukuVolumeItems) <= 0 {
		return
	}

	// If there is more than one KukuVolumeItem with this PVC assigned, show a warning
	if len(kukuVolumeItems) > 1 {
		log.Warnf("%s. Found %d KukuVolumeItem objects with this PVC assigned", meth, len(kukuVolumeItems))
	}

	// Enqueue the KukuVolumeItems
	for _, kukuVolumeItem := range kukuVolumeItems {
		log.Debugf("%s enqueue %s", meth, kukuVolumeItem.GetName())
		c.EnqueueObject(kukuVolumeItem)
	}
}

func (c *Controller) handlePersistentVolume(pv *corev1.PersistentVolume) {
	// Gets the PV name and namespace
	pvname := pv.GetName()
	pvns := pv.GetNamespace()
	meth := fmt.Sprintf("%s.handlePersistentVolume. PersistentVolume: '%s/%s'", c.Name, pvns, pvname)
	log.Debug(meth)

	// Creates the labels filter to find the KukuVolumeItem related with this PV
	matchLabels := map[string]string{
		utils.PersistentVolumeLabel: pvname,
	}
	options := labels.SelectorFromSet(matchLabels)

	// Gets the KukuVolumeItems labeled with this PV
	kukuVolumeItems, err := kviutils.List(c.kukuVolumeItemsLister, c.config.Namespace, options)
	if err != nil && !errors.IsNotFound(err) {
		log.Errorf("%s. Error retrieving KukuVolumeItem: %v", meth, err)
		return
	}

	// Skip if there are not KukuVolumeItems related to this PV
	if kukuVolumeItems == nil || len(kukuVolumeItems) <= 0 {
		return
	}

	// If there is more than one KukuVolumeItem with this PV assigned, show a warning
	if len(kukuVolumeItems) > 1 {
		log.Warnf("%s. Found %d KukuVolumeItem objects with this PV assigned", meth, len(kukuVolumeItems))
	}

	// Enqueue the KukuVolumeItems
	for _, kukuVolumeItem := range kukuVolumeItems {
		log.Debugf("%s enqueue %s", meth, kukuVolumeItem.GetName())
		c.EnqueueObject(kukuVolumeItem)
	}
}

func (c *Controller) handleKukuVolume(kv *kumoriv1.KukuVolume) {

	// Gets the PV name and namespace
	kvname := kv.GetName()
	kvns := kv.GetNamespace()
	meth := fmt.Sprintf("%s.handleKukuVolume. KukuVolume: '%s/%s'", c.Name, kvns, kvname)
	log.Debug(meth)

	// Skips if the volume type is shared
	clusterConfig, err := common.GetClusterConfig(c.configMapLister, &c.config)
	if err != nil {
		log.Errorf("%s. Error reading cluster configuration: %v", meth, err)
		return
	}
	isShared, err := common.IsVolumeTypeShared(kv, clusterConfig)
	if err != nil {
		log.Errorf("%s. Error checking if the KukuVolume is shared: %v", meth, err)
		return
	}
	if isShared {
		log.Debugf("%s. Volume type '%s' is shared. Skipping...", meth, err)
		return
	}
	isVolatile, err := common.IsVolumeTypeVolatile(kv, clusterConfig)
	if err != nil {
		log.Errorf("%s. Error checking if the KukuVolume is volatile: %v", meth, err)
		return
	}
	if isVolatile {
		log.Debugf("%s. Volume type '%s' is volatile. Skipping...", meth, err)
		return
	}

	// Creates the labels filter to find the KukuVolumeItems related with this KukuVolume
	matchLabels := map[string]string{
		utils.KumoriKukuVolumeLabel: kvname,
	}
	options := labels.SelectorFromSet(matchLabels)

	// Gets the KukuVolumeItems labeled with this KukuVolume
	kukuVolumeItems, err := kviutils.List(c.kukuVolumeItemsLister, c.config.Namespace, options)
	if err != nil && !errors.IsNotFound(err) {
		log.Errorf("%s. Error retrieving KukuVolumeItem: %v", meth, err)
		return
	}

	// Skip if there are not KukuVolumeItems related to this KukuVolume
	if kukuVolumeItems == nil || len(kukuVolumeItems) <= 0 {
		return
	}

	// Enqueue the KukuVolumeItems
	for _, kukuVolumeItem := range kukuVolumeItems {
		log.Debugf("%s enqueue %s", meth, kukuVolumeItem.GetName())
		c.EnqueueObject(kukuVolumeItem)
	}
}

// handleConfigMap enqueues all kukuVolumeItems if the cluster configuration changes
func (c *Controller) handleConfigMap(configMap *corev1.ConfigMap) {
	clusterConfigName := c.config.ClusterConfigMapName
	clusterConfigNamespace := c.config.ClusterConfigMapNamespace
	name := configMap.GetName()
	namespace := configMap.GetNamespace()
	if (name == clusterConfigName) && (namespace == clusterConfigNamespace) {
		c.enqueueAllVolumeItems()
	}
}

func (c *Controller) enqueueAllVolumeItems() {
	meth := c.Name + ".enqueueAllVolumeItems()"

	namespace := c.config.Namespace
	matchLabels := map[string]string{}
	options := labels.SelectorFromSet(matchLabels)
	kukuVolumeItems, err := c.kukuVolumeItemsLister.KukuVolumeItems(namespace).List(options)
	if err != nil {
		log.Infof("%s. Cannot get KukuVolumeItems to apply a config file change. Error: %s", meth, err.Error())
	}
	for _, kukuVolumeItem := range kukuVolumeItems {
		c.EnqueueObject(kukuVolumeItem)
	}
}

// SyncHandler reconciles the actual state with the desired state
func (c *Controller) SyncHandler(key string) error {
	meth := c.Name + ".SyncHandler()"
	log.Infof("%s key: %s", meth, key)

	// Converts the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		log.Errorf("%s invalid resource key: %s", meth, key)
		return nil
	}
	log.Debugf("%s Name: %s Namespace: %s ", meth, name, namespace)

	// Gets the KukuVolumeItem
	kukuVolumeItem, err := kviutils.GetByName(c.kukuVolumeItemsLister, namespace, name)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Debugf("%s. KukuVolumeItem '%s/%s' not found. Skipping", meth, namespace, name)
			return nil
		}
		message := fmt.Sprintf("Error getting KukuVolumeItem '%s/%s': %v", namespace, name, err)
		log.Errorf("%s. %s", meth, message)
		return fmt.Errorf(message)
	}

	// If its nil, abort
	if kukuVolumeItem == nil {
		log.Debugf("%s. KukuVolumeItem '%s/%s' not found. Skipping", meth, namespace, name)
		return nil
	}

	// Reconciles the KukuVolumeItem
	if err := c.kukuVolumeItemSyncHandler(kukuVolumeItem); err != nil {
		log.Errorf("%s. %v", meth, err)
		shouldAbort, shouldRetry := kerrors.CheckError(err)
		if shouldRetry || shouldAbort {
			log.Debugf("%s. Requeuing due to: %v", meth, err)
			return err
		}
	}

	return nil
}

// UpdateLogLevel from configuration
func UpdateLogLevel() {
	logLevel := viper.GetString("logLevel")
	log.Infof("Setting log level to %s", logLevel)
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	default:
		log.Warnf("Unknown log level %s. Ignored", logLevel)
	}
}
