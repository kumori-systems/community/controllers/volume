/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitem

import (
	"fmt"
	common "kukuvolume-controller/pkg/controllers/common"
	kerrors "kukuvolume-controller/pkg/utils/errors"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	corev1 "k8s.io/api/core/v1"
)

// runAvailablePhase updates the number of KukuVolumeItems assigned to the KukuVolume. Note that currently
// a KukuVolumeItem is considered in this counter only if it has a PersistentVolume assigned (it is in
// Available, Binding or Bound phase)
func (c *Controller) runAvailablePhase(
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
	kukuVolume *kumoriv1.KukuVolume,
) error {
	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()
	kvname := kukuVolume.GetName()
	kvns := kukuVolume.GetNamespace()

	meth := fmt.Sprintf("%s.runAvailablePhase. KukuVolumeItem: '%s/%s'. KukuVolume: '%s/%s'", c.Name, kvins, kviname, kvns, kvname)
	log.Debugf(meth)

	// If the KukuVolumeItem has not a PVC assigned, abort
	if kukuVolumeItem.Spec.PersistentVolumeClaimRef == "" {
		message := fmt.Sprintf("PVC not assigned. Skipping")
		log.Debugf("%s. %s", meth, message)
		return nil
	}

	// Updates the KukuVolume status phase (to binding) and items
	kukuVolumeItem.Status.Phase = kumoriv1.KukuVolumeItemBindingPhase
	_, err := kviutils.UpdateStatus(c.kumoriClientset, kukuVolumeItem)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s.Phase not updated because the KukuVolumeItem has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
		}
		message := fmt.Sprintf("Error updating phase to 'Binding': %v", err)
		log.Errorf("%s. %s", meth, message)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Error changing phase to Binding: %s", err.Error())
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
	log.Debugf("%s. Phase changed to Binding", meth)
	c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "Phase changed to Binding")

	return nil
}
