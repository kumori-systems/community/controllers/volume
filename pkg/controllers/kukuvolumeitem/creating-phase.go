/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitem

import (
	"fmt"
	common "kukuvolume-controller/pkg/controllers/common"
	"kukuvolume-controller/pkg/utils"
	kerrors "kukuvolume-controller/pkg/utils/errors"
	pvcutils "kukuvolume-controller/pkg/utils/kube/persistentvolumeclaims"
	pvutils "kukuvolume-controller/pkg/utils/kube/persistentvolumes"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
)

// runCreatingPhase changes the KukuPersistentVolume phase to Available only if the KukuVolumeItem has
// a PersitentVolume assigned (this is set by the kuvolume controller)
func (c *Controller) runCreatingPhase(
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) error {
	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()

	meth := fmt.Sprintf("%s.runCreatingPhase. KukuVolumeItem: '%s/%s'", c.Name, kvins, kviname)
	log.Debugf(meth)

	// Checks if there is a temporary PVC for this KukuVolumeItem
	persistentVolumeClaims, err := c.getTemporaryPVCs(kukuVolumeItem)
	if err != nil {
		return err
	}

	// If the KukuVolumeItem has not a PV assigned yet, wait. If it neither exists a temporary PVC then
	// returns the KukuVolumeItem to the initial state
	if kukuVolumeItem.Spec.PersistentVolume == "" {
		if persistentVolumeClaims == nil || len(persistentVolumeClaims) <= 0 {
			// Sets the KukuVolumeItem phase to empty
			kukuVolumeItem.Status.Phase = ""
			_, err := kviutils.UpdateStatus(c.kumoriClientset, kukuVolumeItem)
			if err != nil {
				if common.ErrorIsObjectUpdated(err) {
					message := fmt.Sprintf("%s.Phase not updated because the KukuVolumeItem has been modified", meth)
					log.Debugf(message)
					return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
				}
				message := fmt.Sprintf("Error initializing phase: %v", err)
				log.Errorf("%s. %s", meth, message)
				c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Error changing phase to Initializing: %s", err.Error())
				return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
			}
			log.Debugf("%s. Phase changed to Initializing", meth)
			c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "Phase changed to Initializing")
			return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf("Temporary PVC not found. Changing phase to initial"))
		}

		// If the PVC exists, just wait
		message := fmt.Sprintf("PV not assigned yet")
		log.Debugf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
	}

	// Deletes the temporary PVCs
	failed := false
	for _, tpvc := range persistentVolumeClaims {
		log.Debugf("%s. Deleting temporary PVC '%s/%s'", meth, tpvc.GetNamespace(), tpvc.GetName())
		if err := pvcutils.Delete(c.kubeClientset, tpvc); err != nil {
			log.Errorf("%s. Error removing temporary PVC '%s/%s': %v", meth, tpvc.GetNamespace(), tpvc.GetName(), err)
			c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.DeletedReason, "Temporary pvc '%s/%s' removal failed: %v", tpvc.GetNamespace(), tpvc.GetName(), err.Error())
			failed = true
		}
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.DeletedReason, "Temporary pvc '%s/%s' removed", tpvc.GetNamespace(), tpvc.GetName())
	}
	if failed {
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf("Error removing temporary PVC"))
	}

	// Assigns the KukuVolumeItem PVC to the PV
	persistentVolumeName := kukuVolumeItem.Spec.PersistentVolume
	persistentVolumeClaimName := kukuVolumeItem.Spec.PersistentVolumeClaimRef
	persistentVolume, err := pvutils.GetByName(c.persistentVolumesLister, persistentVolumeName)
	if err != nil {
		message := fmt.Sprintf("Error getting PV '%s': %v", persistentVolumeName, err)
		log.Errorf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	version := corev1.SchemeGroupVersion.Version
	persistentVolume.Spec.ClaimRef = &corev1.ObjectReference{
		Name:       persistentVolumeClaimName,
		Namespace:  c.config.Namespace,
		Kind:       "PersistentVolumeClaim",
		APIVersion: version,
	}
	if _, err = pvutils.Update(c.kubeClientset, persistentVolume); err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s. Claim reference not set because the PersistentVolume has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
		}
		message := fmt.Sprintf("Error updating PV '%s' ClaimRef to '%s/%s: %v", persistentVolumeName, c.config.Namespace, persistentVolumeClaimName, err)
		log.Errorf("%s. %s", meth, message)
		c.Recorder.Eventf(persistentVolume, corev1.EventTypeWarning, common.UpdatedReason, "Error assigning PersistentVolumeClaim '%s': %s", persistentVolumeClaimName, err.Error())
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
	log.Infof("%s. PVC '%s/%s' assigned to PV '%s'", meth, c.config.Namespace, persistentVolumeClaimName, persistentVolumeName)
	c.Recorder.Eventf(persistentVolume, corev1.EventTypeNormal, common.UpdatedReason, "PersistentVolumeClaim '%s' assigned", persistentVolumeClaimName)

	// Sets the KukuVolumeItem phase to Available
	kukuVolumeItem.Status.Phase = kumoriv1.KukuVolumeItemAvailablePhase
	_, err = kviutils.UpdateStatus(c.kumoriClientset, kukuVolumeItem)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s.Phase not updated because the KukuVolumeItem has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
		}
		message := fmt.Sprintf("Error updating phase to 'Available': %v", err)
		log.Errorf("%s. %s", meth, message)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Error changing phase to Available: %s", err.Error())
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
	log.Debugf("%s. Phase changed to Available", meth)
	c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "Phase changed to Available")

	// Everything ook
	return nil
}

// getTemporaryPVCs returns the temporary PVCs assigned to assigned to a given KukuVolumeItem
func (c *Controller) getTemporaryPVCs(
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) ([]*corev1.PersistentVolumeClaim, error) {

	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()

	meth := fmt.Sprintf("%s.getTemporaryPvc. KukuVolumeItem: '%s/%s'", c.Name, kvins, kviname)
	log.Debug(meth)

	// Checks if there is a temporary PVC for this KukuVolumeItem
	matchLabels := map[string]string{
		utils.KumoriKukuVolumeItemLabel: kviname,
	}
	options := labels.SelectorFromSet(matchLabels)
	persistentVolumeClaims, err := c.persistentVolumeClaimsLister.PersistentVolumeClaims(kvins).List(options)
	if err != nil && !errors.IsNotFound(err) {
		message := fmt.Sprintf("Error getting PVC '%s/%s': %v", kvins, kviname, err)
		log.Errorf("%s. %s", meth, message)
		return nil, kerrors.NewKukuError(kerrors.Retry, err)
	}
	if errors.IsNotFound(err) || len(persistentVolumeClaims) <= 0 {
		return nil, nil
	}

	return persistentVolumeClaims, nil
}
