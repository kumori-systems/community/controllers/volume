/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitem

import (
	"fmt"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"

	"kukuvolume-controller/pkg/utils"
	kvutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumes"
)

// updateKukuVolumeItems updates the KukuVolume status
func (c *Controller) updateKukuVolumeItems(kukuVolume *kumoriv1.KukuVolume) (*kumoriv1.KukuVolume, error) {

	kvname := kukuVolume.GetName()
	kvns := kukuVolume.GetNamespace()

	// Gets the KukuVolumeItems of the given KukuVolume
	matchLabels := map[string]string{
		utils.KumoriKukuVolumeLabel: kvname,
	}
	options := labels.SelectorFromSet(matchLabels)
	kukuVolumeItems, err := c.kukuVolumeItemsLister.KukuVolumeItems(kvns).List(options)
	if err != nil && !errors.IsNotFound(err) {
		return kukuVolume, fmt.Errorf("Error getting KukuVolume items: %v", err)
	}

	// Updates the number of items
	numKukuVolumeItems := 0
	if !errors.IsNotFound(err) {
		numKukuVolumeItems = len(kukuVolumeItems)
	}
	kukuVolume.Status.Items = int32(numKukuVolumeItems)

	// Updates the KukuVolume status
	kukuVolume, err = kvutils.UpdateStatus(c.kumoriClientset, kukuVolume)

	// Everything OK
	return kukuVolume.DeepCopy(), nil
}
