/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitem

import (
	"fmt"

	kerrors "kukuvolume-controller/pkg/utils/errors"
	kvutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumes"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
)

// kukuVolumeItemSyncHandler reconciles a KukuVolumeItem
func (c *Controller) kukuVolumeItemSyncHandler(
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) error {
	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()

	meth := fmt.Sprintf("%s.kukuVolumeItemSyncHandler. KukuVolumeItem: '%s/%s'.", c.Name, kviname, kvins)
	log.Debugf(meth)

	// If the KukuVolumeItem is going to be removed, run finalizers to remove the persistent volume (if any)
	if !kukuVolumeItem.GetDeletionTimestamp().IsZero() {

		// Run finalizers.
		err := c.runFinalizers(kukuVolumeItem)
		if err != nil {
			return err
		}

		// Ends the reconciling process since the KukuVolumeItem is going to be removed
		return nil
	}

	kukuVolumeName := kukuVolumeItem.Spec.KukuVolume
	kukuVolume, err := kvutils.GetByName(c.kukuVolumesLister, kvins, kukuVolumeName)
	if errors.IsNotFound(err) {
		message := fmt.Sprintf("KukuVolume '%s/%s' not found.", kvins, kukuVolumeName)
		log.Errorf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
	if err != nil {
		message := fmt.Sprintf("KukuVolume '%s/%s' cannot be retrieved: %v", kvins, kukuVolumeName, err)
		log.Errorf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Updates the KukUVolume status.items
	kukuVolume, err = c.updateKukuVolumeItems(kukuVolume)
	if err != nil {
		message := fmt.Sprintf("Error updating status.items: %v", err)
		log.Errorf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}

	// The next step depends on the phase
	phase := kukuVolumeItem.Status.Phase
	switch phase {
	case "":
		// The KukuVolumeItem has been recently created and it has not a phase or PersistentVolume assigned.
		// A PersistentVolume must be created (using a temporary PersistentVolumeClaim)
		if err := c.runInitPhase(kukuVolumeItem, kukuVolume); err != nil {
			// message := fmt.Sprintf("Error runninge initializing phase: %v", err)
			// log.Errorf("%s. %s", meth, message)
			return err
		}
		// Everything ok
		return nil
	case kumoriv1.KukuVolumeItemCreatingPhase:
		// The temporary PersistentVolumeClaim has been created and we need to check if the PersistentVolume
		// is already bound or not
		if err := c.runCreatingPhase(kukuVolumeItem); err != nil {
			// message := fmt.Sprintf("Error running creating phase: %v", err)
			// log.Errorf("%s. %s", meth, message)
			return err
		}
		// Everything ok
		return nil
	case kumoriv1.KukuVolumeItemAvailablePhase:
		// The KukuPersistentVolume has a PersistentVolume assigned and we need to wait until it is bound
		// to the PersistentVolumeClaim assigned to this KukuVolumeItem (if any)
		if err := c.runAvailablePhase(kukuVolumeItem, kukuVolume); err != nil {
			// message := fmt.Sprintf("Error running available phase: %v", err)
			// log.Errorf("%s. %s", meth, message)
			return err
		}
		// Everything ok
		return nil
	case kumoriv1.KukuVolumeItemBindingPhase:
		// Waits until the PersistentVolume assigned to this KukuVolumeItem is bound to the
		// PersistentVolumeClaim using this KukuVolumeItem.
		if err := c.runBindingPhase(kukuVolumeItem); err != nil {
			// message := fmt.Sprintf("Error running binding phase: %v", err)
			// log.Errorf("%s. %s", meth, message)
			return err
		}
		// Everything ok
		return nil
	case kumoriv1.KukuVolumeItemBoundPhase:
		// The PersistentVolumeClaim and PersitentVolume in this KukuVolumeItem are effectively bound.
		// Waits until the PersistentVolume assigned to this KukuVolumeItem is bound to the
		// PersistentVolumeClaim using this KukuVolumeItem.
		if err := c.runBoundPhase(kukuVolumeItem); err != nil {
			// message := fmt.Sprintf("Error running bound phase: %v", err)
			// log.Errorf("%s. %s", meth, message)
			return err
		}
		// Everything ok
		return nil
	default:
		message := fmt.Sprintf("Unknown phase '%s'", kviname)
		log.Errorf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
}
