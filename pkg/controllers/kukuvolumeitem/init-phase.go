/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitem

import (
	"fmt"
	common "kukuvolume-controller/pkg/controllers/common"
	"kukuvolume-controller/pkg/utils"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"

	kerrors "kukuvolume-controller/pkg/utils/errors"
	pvcutils "kukuvolume-controller/pkg/utils/kube/persistentvolumeclaims"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// runInitPhase initializes a brand new KukuVolumeItem by starting the process of creating its
// PersistentVolume. The KukuVolumeItem has pre-assigned the PersistentVolumeClaim which started
// the KukuPersistentVolume item creation
func (c *Controller) runInitPhase(
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
	kukuVolume *kumoriv1.KukuVolume,
) error {

	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()
	kvname := kukuVolume.GetName()
	kvns := kukuVolume.GetNamespace()

	meth := fmt.Sprintf("%s.runInitStatus. KukuVolumeItem: '%s/%s'. KukuVolume: '%s/%s'", c.Name, kvins, kviname, kvns, kvname)
	log.Debugf(meth)

	// Sets the kukuVolumeItem finalizer to remove the assigned Persistent Volume when the
	// KukuVolumeItem is removed
	log.Debugf("%s. Adding finalizer '%s'", meth, persistentVolumeFinalizerName)
	common.AddFinalizer(kukuVolumeItem, persistentVolumeFinalizerName)
	kukuVolumeItem, err := kviutils.Update(c.kumoriClientset, kukuVolumeItem)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s. Finalizer '%s' has not been added because the KukuVolumeItem has been modified", meth, persistentVolumeFinalizerName)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
		}
		log.Errorf("%s. Error adding finalizer '%s': %v", meth, persistentVolumeFinalizerName, err)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Error adding finalizer '%s': %s", persistentVolumeFinalizerName, err.Error())
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "Finalizer '%s' added", persistentVolumeFinalizerName)
	kukuVolumeItem = kukuVolumeItem.DeepCopy()

	// Checks if already is a temporary PVC for this KukuVolumeItem or if the KukuVolumeItem already
	// has an assigned PV. In this case, skip this step. For some reason, this KukuPersitentVolume item
	// already created the PVC but the status has not been changed to Creating
	pvcAlreadyCreated := (kukuVolumeItem.Spec.PersistentVolume != "")
	if !pvcAlreadyCreated {
		matchLabels := map[string]string{
			utils.KumoriKukuVolumeItemLabel: kviname,
		}
		options := labels.SelectorFromSet(matchLabels)
		persistentVolumeClaims, err := c.persistentVolumeClaimsLister.PersistentVolumeClaims(kvins).List(options)
		if err != nil && !errors.IsNotFound(err) {
			message := fmt.Sprintf("Error getting PVC: %v", err)
			log.Errorf("%s. %s", meth, message)
			return kerrors.NewKukuError(kerrors.Retry, err)
		}
		pvcAlreadyCreated = len(persistentVolumeClaims) > 0
	}

	// Create the PVC if it was not created previously
	if !pvcAlreadyCreated {
		temPvc, err := c.createPersistentVolumeClaim(kukuVolumeItem, kukuVolume)
		if err != nil {
			return err
		}
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.CreatedReason, "Temporary PersistentVolumeClaim '%s/%s' created", temPvc.GetNamespace(), temPvc.GetName())
		log.Debugf("%s. Temporary PVC '%s/%s' created", meth, temPvc.GetNamespace(), temPvc.GetName())
	}

	// Sets the KukuVolumeItem phase to Creating
	kukuVolumeItem.Status.Phase = kumoriv1.KukuVolumeItemCreatingPhase
	_, err = kviutils.UpdateStatus(c.kumoriClientset, kukuVolumeItem)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s.Phase not updated because the KukuVolumeItem has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
		}
		message := fmt.Sprintf("Error updating phase to 'Creating': %v", err)
		log.Errorf("%s. %s", meth, message)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Error changing phase to Creating: %s", err.Error())
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
	log.Debugf("%s. Phase changed to Creating", meth)
	c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "Phase changed to Creating")

	// Everything ook
	return nil
}

// createPersistentVolumeClaim creates a temporary persistent volume claim to fire the creation of
// the PersistentVolume assigned to a given KukuVolumeItem
func (c *Controller) createPersistentVolumeClaim(
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
	kukuVolume *kumoriv1.KukuVolume,
) (*corev1.PersistentVolumeClaim, error) {
	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()
	kvname := kukuVolume.GetName()
	kvns := kukuVolume.GetNamespace()

	meth := fmt.Sprintf("%s.createPersistentVolumeClaim. KukuVolumeItem: '%s/%s'. KukuVolume: '%s/%s'", c.Name, kvins, kviname, kvns, kvname)
	log.Debugf(meth)

	clusterConfig, err := common.GetClusterConfig(c.configMapLister, &c.config)
	if err != nil {
		log.Errorf("%s. Error getting the cluster configuration: %v", meth, err)
		return nil, kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Checks if the cluster configuration contains volume types
	if clusterConfig == nil || clusterConfig.VolumeTypes == nil {
		message := "Volume types not found in cluster configuration"
		log.Errorf("%s. %s", meth, message)
		return nil, kerrors.NewKukuError(kerrors.Abort, fmt.Errorf(message))
	}

	// Gets the volume type
	kukuVolumeType := kukuVolume.Spec.Type

	// Reads the storage class for this volume type
	var storageClassName string
	log.Debugf("%s. Getting '%s' storage class from the cluster configuration", meth, kukuVolumeType)
	volumeType, ok := (*clusterConfig.VolumeTypes)[kukuVolumeType]
	if !ok {
		message := fmt.Sprintf("Storage class not found for KukuVolume type '%s'", kukuVolumeType)
		log.Errorf("%s. %s", meth, message)
		return nil, kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
	storageClassName = volumeType.StorageClass
	log.Debugf("%s. Using storage class '%s' for volume type '%s'", meth, storageClassName, kukuVolumeType)

	// Gets the Persistent Volume size
	kukuVolumeSize := kukuVolume.Spec.Size

	// Generates the labels
	labels := map[string]string{
		utils.KumoriKukuVolumeItemLabel: kviname,
		utils.KumoriControllerLabel:     c.Name,
	}

	// Adds the KukuVolumeItem as an owner of the PVC only in case the PVC removal fails once the
	// PV is created
	group := kumoriv1.SchemeGroupVersion.Group
	version := kumoriv1.SchemeGroupVersion.Version
	references := []metav1.OwnerReference{
		*metav1.NewControllerRef(kukuVolumeItem, schema.GroupVersionKind{
			Group:   group,
			Version: version,
			Kind:    common.KukuVolumeItemKind,
		}),
	}

	// Generates a new PVC object
	pvcname := fmt.Sprintf("%s-pvc", kviname)
	pvc := corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:            pvcname,
			Namespace:       kvins,
			Labels:          labels,
			OwnerReferences: references,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			StorageClassName: &storageClassName,
			AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					"storage": kukuVolumeSize,
				},
			},
		},
	}

	// Adds the new PVC object to the cluster state
	newPvc, err := pvcutils.Create(c.kubeClientset, &pvc)
	if err != nil {
		message := fmt.Sprintf("PVC '%s/%s' creation failed. Error: %v", kvins, pvcname, err)
		log.Errorf("%s. %s", meth, message)
		return nil, kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Returns the temporary PVC
	return newPvc, nil
}
