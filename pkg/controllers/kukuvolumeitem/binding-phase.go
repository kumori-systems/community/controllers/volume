/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitem

import (
	"fmt"
	common "kukuvolume-controller/pkg/controllers/common"
	kerrors "kukuvolume-controller/pkg/utils/errors"
	pvcutils "kukuvolume-controller/pkg/utils/kube/persistentvolumeclaims"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	corev1 "k8s.io/api/core/v1"
)

// runBindingPhase checks if the PersistentVolume and the PersistentVolumeClaim assigned to this
// KukuVolumeItem are bound. It also checks if the PersistentVolumeClaim has been unassigned from this
// KukuVolumeItem.
func (c *Controller) runBindingPhase(
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) error {
	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()

	meth := fmt.Sprintf("%s.runBindingPhase. KukuVolumeItem: '%s/%s'", c.Name, kvins, kviname)
	log.Debugf(meth)

	// If the KukuVolumeItem has not a PVC assigned, change status to Avilable and abort
	if kukuVolumeItem.Spec.PersistentVolumeClaimRef == "" {
		kukuVolumeItem.Status.Phase = kumoriv1.KukuVolumeItemAvailablePhase
		_, err := kviutils.UpdateStatus(c.kumoriClientset, kukuVolumeItem)
		if err != nil {
			if common.ErrorIsObjectUpdated(err) {
				message := fmt.Sprintf("%s.Phase not updated because the KukuVolumeItem has been modified", meth)
				log.Debugf(message)
				return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
			}
			message := fmt.Sprintf("Error updating phase to 'Available': %v", err)
			log.Errorf("%s. %s", meth, message)
			c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Error changing phase to Available: %s", err.Error())
			return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
		}
		log.Debugf("%s. PVC not assigned. Phase changed to Available", meth)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "PVC not assigned. Phase changed to Available")

		return nil
	}

	// Gets the KukuVolumeItem PVC
	pvc, err := pvcutils.GetByName(c.persistentVolumeClaimsLister, c.config.Namespace, kukuVolumeItem.Spec.PersistentVolumeClaimRef)
	if err != nil {
		message := fmt.Sprintf("Error getting PVC '%s/%s': %v", c.config.Namespace, kukuVolumeItem.Spec.PersistentVolumeClaimRef, err)
		log.Errorf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Checks if the PVC has been already bound
	if pvc.Status.Phase != corev1.ClaimBound {
		message := fmt.Sprintf("PVC '%s/%s' still not bound", c.config.Namespace, kukuVolumeItem.Spec.PersistentVolumeClaimRef)
		log.Debugf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
	}

	// Change the KukuVolumeItem phase to Bound
	kukuVolumeItem.Status.Phase = kumoriv1.KukuVolumeItemBoundPhase
	_, err = kviutils.UpdateStatus(c.kumoriClientset, kukuVolumeItem)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s.Phase not updated because the KukuVolumeItem has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
		}
		message := fmt.Sprintf("Error updating phase to 'Bound': %v", err)
		log.Errorf("%s. %s", meth, message)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Error changing phase to Bound: %s", err.Error())
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
	log.Debugf("%s. Phase changed to Bound", meth)
	c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "Phase changed to Bound")

	return nil
}
