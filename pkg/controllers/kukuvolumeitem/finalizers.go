/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitem

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/api/errors"

	kerrors "kukuvolume-controller/pkg/utils/errors"
	pvutils "kukuvolume-controller/pkg/utils/kube/persistentvolumes"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"

	common "kukuvolume-controller/pkg/controllers/common"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	corev1 "k8s.io/api/core/v1"
)

// runFinalizers checks if the kumori/finalizer is still presents and, if it is, removes the
// PV associated to this KukuVolumeItem (if any)
func (c *Controller) runFinalizers(
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) error {
	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()
	meth := fmt.Sprintf("%s.runFinalizers. KukuPersistentVolume: '%s/%s'", c.Name, kvins, kviname)
	log.Debugf(meth)

	// Checks if the KukuVolumeItem has a PV assigned and unassings it if so
	if kukuVolumeItem.Spec.PersistentVolume != "" {
		// Checks if the PersitentVolume still exists
		pvname := kukuVolumeItem.Spec.PersistentVolume
		pv, err := pvutils.GetByName(c.persistentVolumesLister, pvname)

		if err != nil && !errors.IsNotFound(err) {
			log.Errorf("%s. Error retrieving PersistentVolume '%s' from KukuVolumeItem '%s/%s': %v", meth, pvname, kvins, kviname, err)
			return kerrors.NewKukuError(kerrors.Retry, err)
		}

		// Deletes the PersistentVolume if still exists
		if !errors.IsNotFound(err) && pv != nil {

			// Deletes PersistentVolume
			log.Debugf("%s. Deleting persistent volume '%s'", meth, pvname)
			if err = c.deletePersistentVolume(pv); err != nil {
				c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.DeletedReason, "PersistentVolume '%s' deletion failed", pvname)
				return err
			}
			log.Infof("%s. Persistent Volume '%s' deleted", meth, pvname)
			c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.DeletedReason, "PersistentVolume '%s' marked to be deleted", pvname)

			// Skips KVI removal until PV has been completely deleted
			return nil
		}

		// Remove the PersistentVolume reference
		kukuVolumeItem.Spec.PersistentVolume = ""
	}

	// Removes the kukuvolume KukuPersistentVolume finalizer
	log.Debugf("%s. Removing finalizer '%s'", meth, persistentVolumeFinalizerName)
	common.RemoveFinalizer(kukuVolumeItem, persistentVolumeFinalizerName)
	_, err := kviutils.Update(c.kumoriClientset, kukuVolumeItem)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s. Finalizer '%s' has not been deleted because the KukuVolumeItem has been modified", meth, persistentVolumeFinalizerName)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
		}
		log.Errorf("%s. Error deleting finalizer '%s': %v", meth, persistentVolumeFinalizerName, err)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Failed removing finalizer '%s': %s", persistentVolumeFinalizerName, err.Error())
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "Finalizer '%s' removed", persistentVolumeFinalizerName)

	// Everything ok
	return nil
}

// deletePersistentVolume sets the reclaim policy of a PersistentVolume to Delete
func (c *Controller) deletePersistentVolume(pv *corev1.PersistentVolume) error {
	pvname := pv.GetName()
	pvns := pv.GetNamespace()
	meth := fmt.Sprintf("%s.deletePersistentVolume. KukuPersistentVolume: '%s/%s'", c.Name, pvns, pvname)
	log.Debugf(meth)

	// Set reclaim policy to Delete and update
	pv.Spec.PersistentVolumeReclaimPolicy = corev1.PersistentVolumeReclaimDelete
	_, err := pvutils.Update(c.kubeClientset, pv)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s.Reclaim policy not set because the PersistentVolume has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
		}
		log.Errorf("%s. Error updating reclaim policy to delete: %v", meth, err)
		return kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Everything ok
	return nil
}
