/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolume

import (
	"fmt"

	"github.com/fsnotify/fsnotify"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	kerrors "kukuvolume-controller/pkg/utils/errors"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	coreinformers "k8s.io/client-go/informers/core/v1"
	kubeclientset "k8s.io/client-go/kubernetes"
	corelisters "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"

	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	common "kukuvolume-controller/pkg/controllers/common"

	kvutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumes"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
)

// Controller is the controller implementation for V3Deployments.
type Controller struct {
	base.BaseController

	config common.ControllerConfig

	kubeClientset   kubeclientset.Interface
	kumoriClientset kumoriclientset.Interface

	persistentVolumeClaimInformer coreinformers.PersistentVolumeClaimInformer
	kukuVolumeInformer            kumoriinformers.KukuVolumeInformer
	configMapInformer             coreinformers.ConfigMapInformer

	persistentVolumeClaimsLister corelisters.PersistentVolumeClaimLister
	kukuVolumesLister            kumorilisters.KukuVolumeLister
	configMapLister              corelisters.ConfigMapLister
}

// NewController returns a new kuku controller
func NewController(
	kubeClientset kubeclientset.Interface,
	kumoriClientset kumoriclientset.Interface,
	persistentVolumeClaimInformer coreinformers.PersistentVolumeClaimInformer,
	kukuVolumeInformer kumoriinformers.KukuVolumeInformer,
	configMapInformer coreinformers.ConfigMapInformer,
) (c *Controller) {
	meth := "kukuvolume-shared.NewController"
	log.Infof("%s. Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 3)
	syncedFunctions[0] = persistentVolumeClaimInformer.Informer().HasSynced
	syncedFunctions[1] = kukuVolumeInformer.Informer().HasSynced
	syncedFunctions[2] = configMapInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = kukuVolumeInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 3)
	secondarySharedInformers[0] = persistentVolumeClaimInformer.Informer()
	secondarySharedInformers[1] = kukuVolumeInformer.Informer()
	secondarySharedInformers[2] = configMapInformer.Informer()

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     controllerKind,
		Workqueue:                workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), controllerName),
		Recorder:                 base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName),
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}

	configMapLister := configMapInformer.Lister()
	config := common.GetConfig(configMapLister)

	c = &Controller{
		BaseController:                baseController,
		config:                        config,
		kubeClientset:                 kubeClientset,
		kumoriClientset:               kumoriClientset,
		persistentVolumeClaimInformer: persistentVolumeClaimInformer,
		kukuVolumeInformer:            kukuVolumeInformer,
		configMapInformer:             configMapInformer,
		persistentVolumeClaimsLister:  persistentVolumeClaimInformer.Lister(),
		kukuVolumesLister:             kukuVolumeInformer.Lister(),
		configMapLister:               configMapLister,
	}

	c.BaseController.IBaseController = c

	// Watches for changes in configuration. If a change is detected, all KukuVolumeItem objects
	// are queued.
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Infof("%s. Config file changed: %s", meth, e.Name)
		config := common.GetConfig(configMapLister)
		c.config = config
		UpdateLogLevel()
		c.enqueueAllKukuVolumes()
	})
	return
}

// HandleSecondary is in charge of processing changes in the secondary objects
// and force to re-evaluate the primary object
func (c *Controller) HandleSecondary(secondary interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Info(meth)
	obj, kukuVolumeName, err := c.GetObject(secondary)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}

	// TBD: Changes in cluster config configmap

	switch obj := obj.(type) {
	case *corev1.PersistentVolumeClaim:
		// If the object belongs to a kukuVolume, enqueues the kukuVolume
		if kukuVolumeName != "" {

			// TBD: We are interested only in KukuVolumes and PVCs owned by KukuVolumes

			log.Debugf("%s Processing object: %s", meth, obj.GetName())

			// Gets the KukuVolume object
			kukuVolume, err := c.kukuVolumesLister.KukuVolumes(obj.GetNamespace()).Get(kukuVolumeName)
			if errors.IsNotFound(err) {
				log.Infof("%s. Ignoring orphaned object '%s' of KukuVolume '%s'", meth, obj.GetSelfLink(), kukuVolumeName)
				return
			} else if err != nil {
				log.Warnf("%s error retrieving object %s: %v", meth, kukuVolumeName, err)
			}

			// Gets the cluster configuration
			clusterConfig, err := common.GetClusterConfig(c.configMapLister, &c.config)
			if err != nil {
				log.Errorf("%s. Error reading cluster configuration: %v", meth, err)
				return
			}

			// If the cluster configuration not contains information about volume types, abort
			if clusterConfig.VolumeTypes == nil {
				log.Warnf("%s. Volume types configuration not found in cluster configuration. Skipping...", meth)
				return
			}

			// Enqueue the kukuVolume
			c.enqueueKukuVolume(kukuVolume, clusterConfig)
		} else {
			log.Debugf("%s. PersistentVolumeClaim '%s' not belongs to a KukuVolume. Skipping...", meth, obj.GetName())
		}
	case *corev1.ConfigMap:
		// If the cluster configuration have changed, the current KukuVolumes will not be reevaluated but a
		// warning is shown in logs
		name := obj.GetName()
		namespace := obj.GetNamespace()
		if namespace == c.config.ClusterConfigMapNamespace && name == c.config.ClusterConfigMapName {
			log.Warnf("%s. Cluster configuration '%s' has changed. Existing PersistentVolumeClaims will not be reevaluated", meth, obj.GetName())
			// c.enqueueAllKukuVolumes()
		}
	default:
		log.Debugf("%s. Object '%s' is not of interest for this controller. Skipping...", meth, obj.GetName())
	}
}

func (c *Controller) enqueueAllKukuVolumes() {
	meth := c.Name + ".enqueueAllKukuVolumes()"

	// Gets the cluster configuration
	clusterConfig, err := common.GetClusterConfig(c.configMapLister, &c.config)
	if err != nil {
		log.Errorf("%s. Error reading cluster configuration: %v", meth, err)
		return
	}

	// If the cluster configuration not contains information about volume types, abort
	if clusterConfig.VolumeTypes == nil {
		log.Warnf("%s. Volume types configuration not found in cluster configuration. Skipping...", meth)
		return
	}

	// Gets existing KukuVolumes
	namespace := c.config.Namespace
	matchLabels := map[string]string{}
	options := labels.SelectorFromSet(matchLabels)
	kukuVolumes, err := c.kukuVolumesLister.KukuVolumes(namespace).List(options)
	if err != nil {
		log.Infof("%s. Cannot get KukuVolumes to apply a config file change. Error: %s", meth, err.Error())
	}

	// Queues any KukuVolume
	for _, kukuVolume := range kukuVolumes {
		c.enqueueKukuVolume(kukuVolume, clusterConfig)
	}
}

// enqueueKukuVolume enqueues a KukuVolume if it is a shared volume
func (c *Controller) enqueueKukuVolume(kukuVolume *kumoriv1.KukuVolume, clusterConfig *common.ClusterConfig) {
	meth := fmt.Sprintf("%s.enqueueKukuVolume. KukuVolume: %s", c.Name, kukuVolume.GetName())

	// Calculates if this volume is shared or not
	isShared, err := common.IsVolumeTypeShared(kukuVolume, clusterConfig)
	if err != nil {
		log.Errorf("%s. Error reading the volume type properties: %v", meth, err)
		return
	}

	// Skips it if it is not a shared volume
	if !isShared {
		log.Debugf("%s. This volume is not shared. Skipping...", meth)
		return
	}

	// Enqueues the KukuVolume
	log.Debugf("%s enqueue %s", meth, kukuVolume.Name)
	c.EnqueueObject(kukuVolume)

}

// SyncHandler reconciles the actual state with the desired state
func (c *Controller) SyncHandler(key string) error {
	meth := fmt.Sprintf("%s.SyncHandler. Key: %s", c.Name, key)
	log.Infof(meth)

	// Converts the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		log.Errorf("%s. invalid resource", meth)
		return nil
	}
	log.Debugf("%s. Name: %s Namespace: %s ", meth, name, namespace)

	// Gets the KukuVolume
	kukuVolume, err := kvutils.GetByName(c.kukuVolumesLister, namespace, name)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Debugf("%s. KukuVolume '%s/%s' not found. Skipping...", meth, namespace, name)
			return nil
		}
		message := fmt.Sprintf("Error getting KukuVolume '%s/%s': %v", namespace, name, err)
		log.Errorf("%s. %s", meth, message)
		return fmt.Errorf(message)
	}

	// If its nil, abort
	if kukuVolume == nil {
		log.Debugf("%s. KukuVolume '%s/%s' not found. Skipping...", meth, namespace, name)
		return nil
	}

	// Gets the cluster configuration
	clusterConfig, err := common.GetClusterConfig(c.configMapLister, &c.config)
	if err != nil {
		log.Errorf("%s. Error reading cluster configuration: %v", meth, err)
		return err
	}

	// If the cluster configuration not contains information about volume types, abort
	if clusterConfig.VolumeTypes == nil {
		log.Warnf("%s. Volume types configuration not found in cluster configuration. Skipping...", meth)
		return nil
	}

	// Calculates if this volume is shared or not
	isShared, err := common.IsVolumeTypeShared(kukuVolume, clusterConfig)
	if err != nil {
		log.Errorf("%s. Error reading the volume type properties: %v", meth, err)
		return err
	}

	// Skips it if it is not a shared volume
	if !isShared {
		log.Debugf("%s. This volume is not shared. Skipping...", meth)
		return nil
	}

	// Reconciles the KukuVolumeItem
	if err := c.kukuVolumeSyncHandler(kukuVolume); err != nil {
		log.Errorf("%s. %v", meth, err)
		shouldAbort, shouldRetry := kerrors.CheckError(err)
		if shouldRetry || shouldAbort {
			log.Debugf("%s. Requeuing due to: %v", meth, err)
			return err
		}
	}

	return nil
}

// UpdateLogLevel from configuration
func UpdateLogLevel() {
	logLevel := viper.GetString("logLevel")
	log.Infof("Setting log level to %s", logLevel)
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	default:
		log.Warnf("Unknown log level %s. Ignored", logLevel)
	}
}
