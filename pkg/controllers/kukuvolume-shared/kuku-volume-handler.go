/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolume

import (
	"fmt"
	common "kukuvolume-controller/pkg/controllers/common"
	"kukuvolume-controller/pkg/utils"

	kerrors "kukuvolume-controller/pkg/utils/errors"

	pvcutils "kukuvolume-controller/pkg/utils/kube/persistentvolumeclaims"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func (c *Controller) kukuVolumeSyncHandler(
	kukuVolume *kumoriv1.KukuVolume,
) error {
	kvname := kukuVolume.GetName()
	kvns := kukuVolume.GetNamespace()

	meth := fmt.Sprintf("%s.kukuVolumeSyncHandler. KukuVolume: '%s/%s'", c.Name, kvname, kvns)
	log.Debugf(meth)

	// If the kukuVolume is going to be removed, end the reconciling process
	// I think a finalizer is not required
	if !kukuVolume.GetDeletionTimestamp().IsZero() {
		return nil
	}

	// Calculates the PersistentVolumeClaim object paired to this KukuVolume
	newPVC, err := c.calculatePersistentVolumeClaim(kukuVolume)
	if err != nil {
		return err
	}

	// Checks if the pvc already exists. If exists, does nothing. It is unclear what happens if a PVC
	// containing volumes is updated
	currentPVC, err := pvcutils.Get(c.persistentVolumeClaimsLister, newPVC)
	if err != nil {
		if !errors.IsNotFound(err) {
			return kerrors.NewKukuError(kerrors.Retry, err)
		}
	}
	if currentPVC != nil {
		log.Warnf("%s. PersistentVolumeClaim '%s/%s' already exists, Skipping...", meth, kvns, kvname)
		return nil
	}

	// Updates the PVC if exists, creates it if not exists
	createdPVC, operation, err := pvcutils.CreateOrUpdate(c.kubeClientset, c.persistentVolumeClaimsLister, newPVC)
	if err != nil {
		message := fmt.Sprintf("PVC '%s/%s' operation '%s' failed. Error: %v", createdPVC.GetNamespace(), createdPVC.GetName(), string(operation), err)
		log.Errorf("%s. %s", meth, message)
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	if operation == utils.NoneOperation {
		log.Infof("%s. PersistentVolumeClaim '%s/%s' already exists and not requires any update", meth, createdPVC.GetNamespace(), createdPVC.GetName())
	} else {
		log.Infof("%s. PersistentVolumeClaim '%s/%s' %s", meth, createdPVC.GetNamespace(), createdPVC.GetName(), string(operation))
	}

	return nil
}

func (c *Controller) calculatePersistentVolumeClaim(kukuVolume *kumoriv1.KukuVolume) (*corev1.PersistentVolumeClaim, error) {

	kvname := kukuVolume.GetName()
	kvns := kukuVolume.GetNamespace()
	meth := fmt.Sprintf("%s.createPersistentVolumeClaim. KukuVolume: '%s/%s'", c.Name, kvname, kvns)

	// Gets cluster configuration
	clusterConfig, err := common.GetClusterConfig(c.configMapLister, &c.config)
	if err != nil {
		log.Errorf("%s. Error reading cluster configuration: %v", meth, err)
		return nil, kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Checks if the cluster configuration contains volume types
	if clusterConfig == nil || clusterConfig.VolumeTypes == nil {
		message := "Volume types not found in cluster configuration"
		log.Errorf("%s. %s", meth, message)
		return nil, kerrors.NewKukuError(kerrors.Abort, fmt.Errorf(message))
	}

	// Gets the volume type
	kukuVolumeType := kukuVolume.Spec.Type

	// Reads the storage class for this volume type
	var storageClassName string
	log.Debugf("%s. Getting '%s' storage class from the cluster configuration", meth, kukuVolumeType)
	volumeType, ok := (*clusterConfig.VolumeTypes)[kukuVolumeType]
	if !ok {
		message := fmt.Sprintf("Storage class not found for KukuVolume type '%s'", kukuVolumeType)
		log.Errorf("%s. %s", meth, message)
		return nil, kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}
	storageClassName = volumeType.StorageClass
	log.Debugf("%s. Using storage class '%s' for volume type '%s'", meth, storageClassName, kukuVolumeType)

	// Gets the Persistent Volume size
	kukuVolumeSize := kukuVolume.Spec.Size

	// Generates the labels
	labels := map[string]string{
		utils.KumoriKukuVolumeLabel: kvname,
		utils.KumoriControllerLabel: c.Name,
	}

	// Adds the KukuVolume as an owner of the PVC only in case the PVC removal fails once the
	// PV is created
	group := kumoriv1.SchemeGroupVersion.Group
	version := kumoriv1.SchemeGroupVersion.Version
	references := []metav1.OwnerReference{
		*metav1.NewControllerRef(kukuVolume, schema.GroupVersionKind{
			Group:   group,
			Version: version,
			Kind:    common.KukuVolumeKind,
		}),
	}

	// Generates a new PVC object
	pvcname := fmt.Sprintf("%s-shared-pvc", kvname)
	pvc := corev1.PersistentVolumeClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:            pvcname,
			Namespace:       kvns,
			Labels:          labels,
			OwnerReferences: references,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			StorageClassName: &storageClassName,
			AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteMany},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					"storage": kukuVolumeSize,
				},
			},
		},
	}

	// Returns the temporary PVC
	return &pvc, nil
}
