/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolume

import (
	"fmt"

	common "kukuvolume-controller/pkg/controllers/common"
	utils "kukuvolume-controller/pkg/utils"
	kerrors "kukuvolume-controller/pkg/utils/errors"
	pvcutils "kukuvolume-controller/pkg/utils/kube/persistentvolumeclaims"
	pvutils "kukuvolume-controller/pkg/utils/kube/persistentvolumes"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"

	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
)

// runFinalizers removes the PVC from the assigned KukuVolumeItem (if any) when the PVC is going
// to be removed
func (c *Controller) runFinalizers(
	pvc *corev1.PersistentVolumeClaim,
	kukuVolumeName string,
) error {

	pvcname := pvc.GetName()
	pvcns := pvc.GetNamespace()
	meth := fmt.Sprintf("%s.runFinalizers. PersistentVolumeClaim: '%s/%s'. KukuVolumeName: '%s'", c.Name, pvcns, pvcname, kukuVolumeName)

	log.Debugf(meth)

	// Checks if there is a KukuVolumeItem referencing this PVC and removes the reference if so //

	// Calculates the selector to get the KukuVolumeItems of the KukuVolume named as kukuVolumeName.
	matchLabels := map[string]string{
		utils.KumoriKukuVolumeLabel: kukuVolumeName,
	}
	selector := labels.SelectorFromSet(matchLabels)

	// Gets the kukuVolumeItems of the given KukuVolume
	kukuVolumeItems, err := kviutils.List(c.kukuVolumeItemsLister, c.config.Namespace, selector)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Debugf("%s. KukuVolumeItems list is empty. Skipping", meth)
			return nil
		}
		return kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Looks for a KukuVolumeItem in "Available" phase and without a PVC already assigned
	for _, kukuVolumeItem := range kukuVolumeItems {

		// Checks if the KukuVolumeItem is assigned to the provided PVC. If that is the case,
		// remove the reference
		if kukuVolumeItem.Spec.PersistentVolumeClaimRef == pvcname {
			kukuVolumeItem = kukuVolumeItem.DeepCopy()
			kukuVolumeItem.Spec.PersistentVolumeClaimRef = ""
			log.Debugf("%s. Removing PersistentVolumeClaim from KukuVolumeItem '%s/%s'", meth, kukuVolumeItem.GetNamespace(), kukuVolumeItem.GetName())
			if _, err := kviutils.Update(c.kumoriClientset, kukuVolumeItem); err != nil {
				if common.ErrorIsObjectUpdated(err) {
					message := fmt.Sprintf("%s.PersistentVolumeClaim reference has not been unset because the KukuVolumeItem has been modified", meth)
					log.Debugf(message)
					return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
				}
				c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Failed removing PersistentVolumeClaim '%s': %s", pvcname, err.Error())
				return kerrors.NewKukuError(kerrors.Retry, err)
			}
			log.Infof("%s. Removed PersistentVolumeClaim from KukuVolumeItem '%s/%s'", meth, kukuVolumeItem.GetNamespace(), kukuVolumeItem.GetName())
			c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "PersistentVolumeClaim '%s' removed", pvcname)
			break
		}
	}

	// Checks if there is a PersistentVolume referencing this PVC and removes the reference if so //
	pvname := pvc.Spec.VolumeName
	if pvname != "" {
		log.Debugf("%s. Removing claimRef in PersistentVolume '%s'", meth, pvname)
		pv, err := pvutils.GetByName(c.persistentVolumesLister, pvname)
		if err != nil {
			log.Errorf("%s. Error getting PersistentVolume '%s': %v", meth, pvname, err)
			return kerrors.NewKukuError(kerrors.Retry, err)
		}
		pv.Spec.ClaimRef.Name = "mockpvc"
		pv.Spec.ClaimRef.UID = "mockpvc"
		if pv, err = pvutils.Update(c.kubeClientset, pv); err != nil {
			if common.ErrorIsObjectUpdated(err) {
				message := fmt.Sprintf("%s. Claim reference has not been unset because the PersistentVolume has been modified", meth)
				log.Debugf(message)
				return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
			}
			log.Errorf("%s. Error removing claim reference from PersistentVolume '%s': %v", meth, pvname, err)
			c.Recorder.Eventf(pv, corev1.EventTypeWarning, common.UpdatedReason, "Failed removing ClaimRef '%s': %s", pvcname, err.Error())
			return kerrors.NewKukuError(kerrors.Retry, err)
		}
		log.Infof("%s. Removed claimRef in PersistentVolume '%s'", meth, pvname)
		c.Recorder.Eventf(pv, corev1.EventTypeNormal, common.UpdatedReason, "ClaimRef '%s' removed", pvcname)
	}

	// Removes the PersistentVolumeClaim finalizer
	log.Debugf("%s. Removing finalizer '%s'", meth, persistentVolumeClaimFinalizerName)
	common.RemoveFinalizer(pvc, persistentVolumeClaimFinalizerName)
	// pvc.Spec.VolumeName = ""
	_, err = pvcutils.Update(c.kubeClientset, pvc)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s. Finalizer '%s' has not been deleted because the PersistentVolumeClaim has been modified", meth, persistentVolumeClaimFinalizerName)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
		}
		log.Errorf("%s. Error deleting finalizer '%s': %v", meth, persistentVolumeClaimFinalizerName, err)
		c.Recorder.Eventf(pvc, corev1.EventTypeWarning, common.UpdatedReason, "Failed removing finalizer '%s': %s", persistentVolumeClaimFinalizerName, err.Error())
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	c.Recorder.Eventf(pvc, corev1.EventTypeNormal, common.UpdatedReason, "Finalizer '%s' removed", persistentVolumeClaimFinalizerName)

	// Everything ok
	return nil
}
