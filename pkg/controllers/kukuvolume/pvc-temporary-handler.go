/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolume

import (
	"fmt"

	common "kukuvolume-controller/pkg/controllers/common"
	utils "kukuvolume-controller/pkg/utils"
	kerrors "kukuvolume-controller/pkg/utils/errors"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"

	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
)

func (c *Controller) temporaryPVCSyncHandler(
	pvc *corev1.PersistentVolumeClaim,
	kukuVolumeItemName string,
) error {

	pvcname := pvc.GetName()
	pvcns := pvc.GetNamespace()

	meth := fmt.Sprintf("%s.temporaryPVCSyncHandler. PeristentVolumeClaim: '%s/%s'. KukuVolumeItem: '%s'", c.Name, pvcns, pvcname, kukuVolumeItemName)

	log.Debugf(meth)

	// If the PVC is not bound then it has not a PV assigned yet. Requeue PVC
	if pvc.Status.Phase != corev1.ClaimBound {
		message := fmt.Sprintf("PVC not bound. Skipping")
		log.Debugf("%s. %s", meth, message)
		// return fmt.Errorf(message)
		// Don't return an error. The PVC will be queued again once it phase changes.
		return nil
	}

	volumeName := pvc.Spec.VolumeName

	// Gets the KukuVolumeItem assigned to this PVC
	kukuVolumeItem, err := kviutils.GetByName(c.kukuVolumeItemsLister, c.config.Namespace, kukuVolumeItemName)
	if err != nil {
		log.Errorf("%s. Error reading KukuVolumeItem: %v", meth, err)
		return kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Changes the spec.PersistentVolume using the PV name
	kukuVolumeItem.Spec.PersistentVolume = volumeName
	kukuVolumeItem.Labels[utils.PersistentVolumeLabel] = volumeName
	kukuVolumeItem, err = kviutils.Update(c.kumoriClientset, kukuVolumeItem)
	if err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s.PersistentVolume not assigned because the KukuVolumeItem has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
		}
		message := fmt.Sprintf("Error assigning PersistentVolume '%s': %v", volumeName, err)
		log.Errorf("%s. %s", meth, message)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Error assigning PersistentVolume '%s': %s", volumeName, err.Error())
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	log.Infof("%s. PV '%s' assigned", meth, volumeName)
	c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "PersistentVolume '%s' assigned", volumeName)

	return nil
}
