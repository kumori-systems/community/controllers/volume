/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolume

import (
	"fmt"
	"strings"

	common "kukuvolume-controller/pkg/controllers/common"
	kutils "kukuvolume-controller/pkg/utils"
	utils "kukuvolume-controller/pkg/utils"
	kerrors "kukuvolume-controller/pkg/utils/errors"
	pvcutils "kukuvolume-controller/pkg/utils/kube/persistentvolumeclaims"
	pvutils "kukuvolume-controller/pkg/utils/kube/persistentvolumes"
	podutils "kukuvolume-controller/pkg/utils/kube/pods"
	kviutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumeitems"
	kvutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumes"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func (c *Controller) kucontrollerPVCSyncHandler(
	pvc *corev1.PersistentVolumeClaim,
	kukuVolumeName string,
) error {

	pvcname := pvc.GetName()
	pvcns := pvc.GetNamespace()

	meth := fmt.Sprintf("%s.kucontrollerPVCSyncHandler. PersistentVolumeClaim: '%s/%s. KukuVolume: '%s'", c.Name, pvcns, pvcname, kukuVolumeName)

	log.Debugf(meth)

	// If the PersistentVolumeClaim is going to be removed, run finalizers to remove the PVC from
	// the assigned KukuVolumeItem (if any)
	if !pvc.GetDeletionTimestamp().IsZero() {

		// Look for a Pod using this PVC. It there is any, re-enqueue the object.
		// We only want to run our finalizers once the related Pod is actually destroyed.

		// Determine Pod name from PVC name: PVC name is '<volumeID>-<pod_name>
		firstDashPos := strings.Index(pvc.Name, "-")
		podName := pvc.Name[firstDashPos+1:]

		pod, err := podutils.GetByName(c.podLister, podName)

		// TRACES FOR DEBUGGING
		// fmt.Println("ERR: ", err)
		// if pod != nil {
		// 	fmt.Println("POD: ", pod.Name)
		// }

		if err != nil {
			if strings.HasSuffix(err.Error(), "not found") {
				message := fmt.Sprintf("%s. No Pod found using the PVC, proceed with finalizers.", meth)
				log.Debugf(message)
			} else {
				message := fmt.Sprintf("%s. Unable to determine if a Pod is using the PVC. Requeueing. Error: %s", meth, err.Error())
				log.Debugf(message)
				return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
			}
		}

		if pod != nil {
			// A Pod using the PVC still exists, requeue
			// NOTE: to avoid a hot-loop, the re-enqueueing should be delayed (not currently supported)
			message := fmt.Sprintf("%s. A Pod still exists that uses the PVC, wait for it to be deleted. Requeueing.", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
		}

		// Run finalizers.
		err = c.runFinalizers(pvc, kukuVolumeName)
		if err != nil {
			return err
		}

		// Ends the reconciling process since the PersistentVolumeClaim is going to be removed
		return nil
	}

	// Sets the controller finalizer if needed. If the finalizer is added, the reconciliation process
	// is aborted to avoid the race condition described in ticket 702. The PVC will be requeued again
	// because it has been updated adding the finalizer.
	if !common.ContainsFinalizer(pvc, persistentVolumeClaimFinalizerName) {
		log.Debugf("%s. Adding finalizer '%s'", meth, persistentVolumeClaimFinalizerName)
		common.AddFinalizer(pvc, persistentVolumeClaimFinalizerName)
		pvc, err := pvcutils.Update(c.kubeClientset, pvc)
		if err != nil {
			if common.ErrorIsObjectUpdated(err) {
				message := fmt.Sprintf("%s. Finalizer '%s' has not been added because the PersistentVolumeClaim has been modified", meth, persistentVolumeClaimFinalizerName)
				log.Debugf(message)
				return kerrors.NewKukuError(kerrors.Wait, fmt.Errorf(message))
			}
			log.Errorf("%s. Error adding finalizer '%s': %v", meth, persistentVolumeClaimFinalizerName, err)
			c.Recorder.Eventf(pvc, corev1.EventTypeWarning, common.UpdatedReason, "Failed adding finalizer '%s': %s", persistentVolumeClaimFinalizerName, err.Error())
			return kerrors.NewKukuError(kerrors.Retry, err)
		}
		pvc = pvc.DeepCopy()
		c.Recorder.Eventf(pvc, corev1.EventTypeNormal, common.UpdatedReason, "Finalizer '%s' added", persistentVolumeClaimFinalizerName)
		log.Debugf("%s. Finalizer '%s' added", meth, persistentVolumeClaimFinalizerName)
		return nil
	}

	// If the PVC is already bound, abort
	if pvc.Status.Phase == corev1.ClaimBound {
		message := fmt.Sprintf("PersistentVolumeClaim already bound. Ignoring")
		log.Debugf("%s.%s", meth, message)
		return nil
	}

	// Calculates the selector to get the KukuVolumeItems of the KukuVolume named as kukuVolumeName.
	matchLabels := map[string]string{
		utils.KumoriKukuVolumeLabel: kukuVolumeName,
	}
	selector := labels.SelectorFromSet(matchLabels)

	// Gets the kukuVolumeItems of the given KukuVolume
	kukuVolumeItems, err := kviutils.List(c.kukuVolumeItemsLister, c.config.Namespace, selector)
	if err != nil && !errors.IsNotFound(err) {
		return kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Gets the KukuVolumeItem assigned to this PVC, if any
	kukuVolumeItem, err := c.findPVCKukuVolumeItem(pvc, kukuVolumeItems)
	if err != nil {
		return err
	}

	// A KukuVolumeItem is already assigned to the PVC. Nothing to do.
	if kukuVolumeItem != nil {
		log.Debugf("%s. PersistentVolumeClaim already assigned to KukuVolumeItem '%s/%s'. Skipping", meth, kukuVolumeItem.GetNamespace(), kukuVolumeItem.GetName())
		return nil
	}

	// Looks for a KukuVolumeItem in "Available" phase and without a PVC already assigned
	for _, kukuVolumeItem := range kukuVolumeItems {

		// Skips the KukuVolumeItem if it isn't in Available phase
		if kukuVolumeItem.Status.Phase != kumoriv1.KukuVolumeItemAvailablePhase {
			continue
		}

		// Skips the KukuVolumeItem if it has a PVC already assigned
		if kukuVolumeItem.Spec.PersistentVolumeClaimRef != "" {
			continue
		}

		// Make a copy, since we may modify it
		kukuVolumeItem = kukuVolumeItem.DeepCopy()

		kvins := kukuVolumeItem.GetNamespace()
		kviname := kukuVolumeItem.GetName()

		// Assigns the kukuVolumeItem to the PVC. If something wrong happens, abort
		if err = c.kukuVolumeItemAvailableSyncHandler(pvc, kukuVolumeItem); err != nil {
			log.Warnf("%s. KukuVolumeItem '%s/%s' has not been assigned due to error: %v", meth, kvins, kviname, err)
			return err
		}

		log.Infof("%s. KukuVolumeItem '%s/%s' assigned to PVC '%s/%s'", meth, kvins, kviname, pvcns, pvcname)
		return nil
	}

	// Otherwise, it not exists a KukuVolumeItem available or already assigned to this PVC. A new one
	// must be created (if possible)
	newKukuVolumeItem, err := c.kukuVolumeItemNotAvailableSyncHandler(pvc, kukuVolumeName, kukuVolumeItems)
	if err != nil {
		log.Warnf("%s. Error creating a new KukuVolumeItem: %v", meth, err)
		return err
	}

	// KukuVolumeItem properly created
	log.Infof("%s. KukuVolumeItem '%s/%s' created", meth, newKukuVolumeItem.GetNamespace(), newKukuVolumeItem.GetName())
	return nil
}

// kukuVolumeItemAvailableSyncHandler reconciles a PVC when it has not any KukuVolumeItem assigned but
// there are KukuVolumeItems in Available phase and without a PVC assigned
func (c *Controller) kukuVolumeItemAvailableSyncHandler(
	pvc *corev1.PersistentVolumeClaim,
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) error {

	pvcname := pvc.GetName()
	pvcns := pvc.GetNamespace()
	kviname := kukuVolumeItem.GetName()
	kvins := kukuVolumeItem.GetNamespace()

	meth := fmt.Sprintf("%s.kukuVolumeItemAvailable. PersistentVolumeClaim: '%s/%s'. KukuVolumeItem: '%s/%s'", c.Name, pvcns, pvcname, kvins, kviname)

	log.Debugf(meth)

	// Gets the PV assigned to this KukuVolumeItem
	pvname := kukuVolumeItem.Spec.PersistentVolume
	pv, err := pvutils.GetByName(c.persistentVolumesLister, pvname)
	if err != nil {
		log.Errorf("%s. The KukuVolumeItem is in available phase but its PersistentVolume '%s' cannot be retrieved. Error: %v", meth, pvname, err)
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	if pv == nil {
		message := fmt.Sprintf("%s. The KukuVolumeItem is in available phase but its PersistentVolume '%s' cannot be retrieved. Error: not found", meth, pvname)
		log.Errorf(message)
		return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
	}

	// Assigns the PVC to this KukuVolumeItem
	kukuVolumeItem.Spec.PersistentVolumeClaimRef = pvcname
	kukuVolumeItem.Labels[utils.PersistentVolumeClaimLabel] = pvcname
	if _, err := kviutils.Update(c.kumoriClientset, kukuVolumeItem); err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s. PersistentVolumeClaim reference has not been set because the KukuVolumeItem has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
		}
		message := fmt.Sprintf("%s. PersistentVolumeClaim reference has not been set. Error: %s", meth, err.Error())
		log.Warn(message)
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Failed assigning persistentVolumeClaim '%s': %s", pvcname, err.Error())
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "PersistentVolumeClaim '%s' assigned", pvcname)

	message := fmt.Sprintf("%s. PersistentVolumeClaim reference set successfully.", meth)
	log.Debugf(message)

	// Assigns this PVC to the retrieved PV
	pv.Spec.ClaimRef = &corev1.ObjectReference{
		Kind:            pvc.Kind,
		Namespace:       pvcns,
		Name:            pvcname,
		UID:             pvc.GetUID(),
		APIVersion:      pvc.APIVersion,
		ResourceVersion: pvc.GetResourceVersion(),
	}

	if _, err := pvutils.Update(c.kubeClientset, pv); err != nil {
		if common.ErrorIsObjectUpdated(err) {
			message := fmt.Sprintf("%s. Claim reference has not been assigned because the PersistentVolume has been modified", meth)
			log.Debugf(message)
			return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
		} else {
			message := fmt.Sprintf("%s. Claim reference has not been assigned. Error: %s", meth, err.Error())
			log.Warn(message)
		}
		c.Recorder.Eventf(pv, corev1.EventTypeWarning, common.UpdatedReason, "Failed assigning persistentVolumeClaim '%s': %s", pvcname, err.Error())
		// If something wrong happens, unsets the PVC to the KukuVolumeItem
		kukuVolumeItem.Spec.PersistentVolumeClaimRef = ""
		kukuVolumeItem.Labels[utils.PersistentVolumeClaimLabel] = ""
		if _, err2 := kviutils.Update(c.kumoriClientset, kukuVolumeItem); err2 != nil {
			if common.ErrorIsObjectUpdated(err) {
				message := fmt.Sprintf("%s. PersistentVolumeClaim reference not unset because the KukuVolumeItem has been modified", meth)
				log.Debugf(message)
				return kerrors.NewKukuError(kerrors.Retry, fmt.Errorf(message))
			}
			log.Errorf("%s. Error unseting the PersistentVolumeClaimRef: %v", meth, err2)
			c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeWarning, common.UpdatedReason, "Failed unassigning persistentVolumeClaim '%s': %s", pvcname, err2.Error())
			return kerrors.NewKukuError(kerrors.Retry, err)
		}
		c.Recorder.Eventf(kukuVolumeItem, corev1.EventTypeNormal, common.UpdatedReason, "PersistentVolumeClaim '%s' unassigned: %s", pvcname, err.Error())
		return kerrors.NewKukuError(kerrors.Retry, err)
	}
	c.Recorder.Eventf(pv, corev1.EventTypeNormal, common.UpdatedReason, "PersistentVolumeClaim '%s' assigned", pvcname)

	return nil
}

// kukuVolumeItemNotAvailableSyncHandler reconciles a PVC when it has not any KukuVolumeItem assigned
// and there isn't any unassigned in Available phase
func (c *Controller) kukuVolumeItemNotAvailableSyncHandler(
	pvc *corev1.PersistentVolumeClaim,
	kukuVolumeName string,
	kukuVolumeItems []*kumoriv1.KukuVolumeItem,
) (*kumoriv1.KukuVolumeItem, error) {

	pvcname := pvc.GetName()
	pvcns := pvc.GetNamespace()

	meth := fmt.Sprintf("%s.kukuVolumeItemNotAvailableSyncHandler. PersistentVolumeClaim: '%s/%s'. KukuVolume: '%s'", c.Name, pvcns, pvcname, kukuVolumeName)

	log.Debugf(meth)

	// Gets the KumoriVolume of the PVC
	kukuVolume, err := kvutils.GetByName(c.kukuVolumesLister, c.config.Namespace, kukuVolumeName)
	if err != nil {
		return nil, kerrors.NewKukuError(kerrors.Retry, err)
	}

	// Gets the maximum amount of items allowed
	if kukuVolume.Spec.Items != nil && (*kukuVolume.Spec.Items > 0) {
		existingItems := int32(len(kukuVolumeItems))
		if existingItems >= *kukuVolume.Spec.Items {
			c.Recorder.Eventf(kukuVolume, corev1.EventTypeNormal, common.CreatedReason, "KukuVolumeItem cannot be created. Maximum reached")
			err := fmt.Errorf("KukuVolumeItem cannot be created: the KukuVolume maximum number of items reached")
			return nil, kerrors.NewKukuError(kerrors.Retry, err)
		}
	}

	// Calculates the new KukuVolumeItem name as the combination of the KukuVolumeName and a random
	// string
	randSuffix := kutils.RandString(5)
	kukuVolumeItemName := fmt.Sprintf("%s-%s", kukuVolumeName, randSuffix)

	// Includes the kukuVolume as an owner of the KukuVolumeItem
	group := kumoriv1.SchemeGroupVersion.Group
	version := kumoriv1.SchemeGroupVersion.Version
	references := []metav1.OwnerReference{
		*metav1.NewControllerRef(kukuVolume, schema.GroupVersionKind{
			Group:   group,
			Version: version,
			Kind:    common.KukuVolumeKind,
		}),
	}

	// Creates the KukuVolumeItem labels including a label to store the KukuVolume name and
	// a label indicating the controller managing this element
	labels := map[string]string{
		utils.KumoriControllerLabel:      controllerName,
		utils.KumoriKukuVolumeLabel:      kukuVolumeName,
		utils.PersistentVolumeClaimLabel: pvcname,
	}

	// Creates the KukuVolumeItem
	// NOTA: algo no está bien porque no debería definir el TypeMeta. No se hace con otros objetos
	kukuVolumeItem := kumoriv1.KukuVolumeItem{
		TypeMeta: metav1.TypeMeta{
			Kind:       common.KukuVolumeItemKind,
			APIVersion: "kumori.systems/v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:            kukuVolumeItemName,
			Namespace:       c.config.Namespace,
			Labels:          labels,
			OwnerReferences: references,
		},
		Spec: kumoriv1.KukuVolumeItemSpec{
			KukuVolume:               kukuVolumeName,
			PersistentVolume:         "",
			PersistentVolumeClaimRef: pvcname,
		},
	}

	// content, _ := json.MarshalIndent(kukuVolumeItem, "", "  ")
	// fmt.Printf("\n\n------->KUKUVOLUMEITEM: %s", string(content))

	// Creates the new KukuVolumeItem and returns the result
	createdKukuVolumeItem, err := kviutils.Create(c.kumoriClientset, &kukuVolumeItem)
	if err != nil {
		c.Recorder.Eventf(kukuVolume, corev1.EventTypeWarning, common.UpdatedReason, "Error creating KukuVolumeItem '%s/%s': %s", kukuVolumeItem.GetNamespace(), kukuVolumeItem.GetName(), err.Error())
		return createdKukuVolumeItem, kerrors.NewKukuError(kerrors.Retry, err)
	}
	c.Recorder.Eventf(kukuVolume, corev1.EventTypeNormal, common.UpdatedReason, "KukuVolumeItem '%s/%s' created", kukuVolumeItem.GetNamespace(), kukuVolumeItem.GetName())
	return createdKukuVolumeItem, nil
}

// findPVCKukuVolumeItem looks for a KukuVolumeItem already assigned to this PVC
func (c *Controller) findPVCKukuVolumeItem(
	pvc *corev1.PersistentVolumeClaim,
	kukuVolumeItems []*kumoriv1.KukuVolumeItem,
) (*kumoriv1.KukuVolumeItem, error) {
	// meth := fmt.Sprintf("%s.findPVCKukuVolumeItem", c.Name)

	// Returns nil if the KukuItemsList is empty
	if (kukuVolumeItems == nil) || (len(kukuVolumeItems) <= 0) {
		return nil, nil
	}

	// Checks if there is a KukuVolumeItem already assigned to this PVC
	pvcname := pvc.GetName()
	// pvcns := pvc.GetNamespace()
	for _, kukuVolumeItem := range kukuVolumeItems {
		if kukuVolumeItem.Spec.PersistentVolumeClaimRef == pvcname {
			// kviname := pvc.GetName()
			// kvins := pvc.GetNamespace()
			// log.Debugf("%s. PVC '%s/%s' has KukuVolumeItem '%s/%s' assigned", meth, pvcns, pvcname, kvins, kviname)
			return kukuVolumeItem.DeepCopy(), nil
		}
	}

	// Returns nil if a KukuVolumeItem assigned to the PVC is not found
	// log.Debugf("%s. PVC '%s/%s' has not a KukuVolumeItem assigned", meth, pvcns, pvcname)
	return nil, nil
}
