/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolume

import (
	"fmt"

	"github.com/fsnotify/fsnotify"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	coreinformers "k8s.io/client-go/informers/core/v1"
	kubeclientset "k8s.io/client-go/kubernetes"
	corelisters "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"

	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	common "kukuvolume-controller/pkg/controllers/common"
	utils "kukuvolume-controller/pkg/utils"
	kerrors "kukuvolume-controller/pkg/utils/errors"
	pvcutils "kukuvolume-controller/pkg/utils/kube/persistentvolumeclaims"
	kvutils "kukuvolume-controller/pkg/utils/kumori/kukuvolumes"
)

// Controller is the controller implementation for V3Deployments.
type Controller struct {
	base.BaseController

	config common.ControllerConfig

	kubeClientset   kubeclientset.Interface
	kumoriClientset kumoriclientset.Interface

	persistentVolumeInformer      coreinformers.PersistentVolumeInformer
	persistentVolumeClaimInformer coreinformers.PersistentVolumeClaimInformer
	kukuVolumeInformer            kumoriinformers.KukuVolumeInformer
	kukuVolumeItemInformer        kumoriinformers.KukuVolumeItemInformer
	podInformer                   coreinformers.PodInformer
	configMapInformer             coreinformers.ConfigMapInformer

	persistentVolumesLister      corelisters.PersistentVolumeLister
	persistentVolumeClaimsLister corelisters.PersistentVolumeClaimLister
	kukuVolumesLister            kumorilisters.KukuVolumeLister
	kukuVolumeItemsLister        kumorilisters.KukuVolumeItemLister
	podLister                    corelisters.PodLister
	configMapLister              corelisters.ConfigMapLister
}

// NewController returns a new kuku controller
func NewController(
	kubeClientset kubeclientset.Interface,
	kumoriClientset kumoriclientset.Interface,
	persistentVolumeInformer coreinformers.PersistentVolumeInformer,
	persistentVolumeClaimInformer coreinformers.PersistentVolumeClaimInformer,
	kukuVolumeInformer kumoriinformers.KukuVolumeInformer,
	kukuVolumeItemInformer kumoriinformers.KukuVolumeItemInformer,
	podInformer coreinformers.PodInformer,
	configMapInformer coreinformers.ConfigMapInformer,
) (c *Controller) {
	meth := "kukuvolume.NewController"
	log.Infof("%s. Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 6)
	syncedFunctions[0] = persistentVolumeInformer.Informer().HasSynced
	syncedFunctions[1] = persistentVolumeClaimInformer.Informer().HasSynced
	syncedFunctions[2] = kukuVolumeInformer.Informer().HasSynced
	syncedFunctions[3] = kukuVolumeItemInformer.Informer().HasSynced
	syncedFunctions[4] = podInformer.Informer().HasSynced
	syncedFunctions[5] = configMapInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 0)

	secondarySharedInformers := make([]cache.SharedIndexInformer, 5)
	secondarySharedInformers[0] = persistentVolumeInformer.Informer()
	secondarySharedInformers[1] = persistentVolumeClaimInformer.Informer()
	secondarySharedInformers[2] = kukuVolumeInformer.Informer()
	secondarySharedInformers[3] = kukuVolumeItemInformer.Informer()
	secondarySharedInformers[4] = configMapInformer.Informer()

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     controllerKind,
		Workqueue:                workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), controllerName),
		Recorder:                 base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName),
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}

	configMapLister := configMapInformer.Lister()
	config := common.GetConfig(configMapLister)

	c = &Controller{
		BaseController:                baseController,
		config:                        config,
		kubeClientset:                 kubeClientset,
		kumoriClientset:               kumoriClientset,
		persistentVolumeInformer:      persistentVolumeInformer,
		persistentVolumeClaimInformer: persistentVolumeClaimInformer,
		kukuVolumeInformer:            kukuVolumeInformer,
		kukuVolumeItemInformer:        kukuVolumeItemInformer,
		podInformer:                   podInformer,
		configMapInformer:             configMapInformer,
		persistentVolumesLister:       persistentVolumeInformer.Lister(),
		persistentVolumeClaimsLister:  persistentVolumeClaimInformer.Lister(),
		kukuVolumesLister:             kukuVolumeInformer.Lister(),
		kukuVolumeItemsLister:         kukuVolumeItemInformer.Lister(),
		podLister:                     podInformer.Lister(),
		configMapLister:               configMapLister,
	}

	c.BaseController.IBaseController = c

	// Watches for changes in configuration. If a change is detected, all V3Deployment objects
	// are queued.
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Infof("%s. Config file changed: %s", meth, e.Name)
		config := common.GetConfig(configMapLister)
		c.config = config
		UpdateLogLevel()
		namespace := config.Namespace
		kumoriReq, err := labels.NewRequirement(utils.KumoriKukuVolumeLabel, selection.Exists, []string{})
		var options labels.Selector
		if err != nil {
			log.Errorf("%s. Error creating selector: %v", meth, err)
			// matchLabels := map[string]string{}
			// options = labels.SelectorFromSet(matchLabels)
			return
		} else {
			options = labels.NewSelector()
			options.Add(*kumoriReq)
		}
		persistentVolumeClaims, err := c.persistentVolumeClaimsLister.PersistentVolumeClaims(namespace).List(options)
		if err != nil {
			log.Infof("%s. Cannot get PersistentVolumeClaims to apply a config file change. Error: %v", meth, err)
		}
		for _, persistentVolumeClaim := range persistentVolumeClaims {
			c.handlePersistentVolumeClaim(persistentVolumeClaim)
		}
	})
	return
}

// HandleSecondary is in charge of processing changes in the secondary objects
// and force to re-evaluate the primary object
func (c *Controller) HandleSecondary(secondary interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Info(meth)
	// obj, kukuVolumeName, err := c.GetObject(secondary)
	obj, _, err := c.GetObject(secondary)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}

	switch obj := obj.(type) {
	case *corev1.PersistentVolumeClaim:
		c.handlePersistentVolumeClaim(obj)
	default:
		// Nothing
		// if kukuVolumeName != "" {
		// 	log.Debugf("%s Processing object: %s", meth, obj.GetName())
		// 	kukuVolume, err := c.kukuVolumesLister.KukuVolumes(obj.GetNamespace()).Get(kukuVolumeName)
		// 	if errors.IsNotFound(err) {
		// 		log.Infof("%s ignoring orphaned object '%s' of kukuhttpinbound '%s'", meth, obj.GetSelfLink(), kukuVolumeName)
		// 		return
		// 	} else if err != nil {
		// 		log.Warnf("%s error retrieving object %s: %v", meth, kukuVolumeName, err)
		// 	}
		// 	log.Debugf("%s enqueue %s", meth, kukuVolume.Name)
		// 	c.EnqueueObject(kukuVolume)
		// }
	}
}

// SyncHandler reconciles the actual state with the desired state
func (c *Controller) SyncHandler(key string) error {
	meth := fmt.Sprintf("%s.SyncHandler(). Key: '%s'", c.Name, key)
	log.Infof(meth)

	// Convert the namespace/name string into a distinct namespace and name
	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		log.Errorf("%s. Invalid resource", meth)
		return nil
	}
	log.Debugf("%s. Name: %s Namespace: %s ", meth, name, namespace)

	// Get the PVC object
	pvc, err := pvcutils.GetByName(c.persistentVolumeClaimsLister, namespace, name)
	if err != nil {
		// If the PVC has not been found then it has been removed. Skip it
		if errors.IsNotFound(err) {
			log.Debugf("%s. PersistentVolumeClaim not found. Skipping", meth)
			return nil
		}
		message := fmt.Sprintf("Error getting PersistentVolumeClaim : %v", err)
		log.Errorf("%s. %s", meth, message)
		return fmt.Errorf(message)
	}
	if pvc == nil {
		log.Debugf("%s. PersistentVolumeClaim not found. Skipping", meth)
		return nil
	}

	// If the PVC includes the kumori/kukuvolume label then it is a PVC generated by
	// kucontroller
	labels := pvc.GetLabels()
	if kukuVolumeName, ok := labels[utils.KumoriKukuVolumeLabel]; ok && kukuVolumeName != "" {

		if err := c.kucontrollerPVCSyncHandler(pvc, kukuVolumeName); err != nil {
			shouldAbort, shouldRetry := kerrors.CheckError(err)
			if shouldRetry || shouldAbort {
				log.Debugf("%s. Requeuing kucontroller PersistentVolumeClaim due to: %v", meth, err)
				return err
			}
		}
		return nil
	}

	// If the PVC includes the kumori/kukuvolumeitem label then it is a PVC generated by
	// this KukuVolumeItem controller
	if kukuVolumeItemName, ok := labels[utils.KumoriKukuVolumeItemLabel]; ok && kukuVolumeItemName != "" {
		if err := c.temporaryPVCSyncHandler(pvc, kukuVolumeItemName); err != nil {
			shouldAbort, shouldRetry := kerrors.CheckError(err)
			if shouldRetry || shouldAbort {
				log.Debugf("%s. Requeuing temporary PersistentVolumeClaim due to: %v", meth, err)
				return err
			}
		}
		return nil
	}

	// If this point is reached, the PVC is not generated by kucontroller, neither by KukuVolumeItems
	// controller and has been queued by mistake.
	log.Warnf("%s. PVC '%s/%s' is not associated to a KukuVolume. Skipping", meth, namespace, name)
	return nil
}

// UpdateLogLevel from configuration
func UpdateLogLevel() {
	logLevel := viper.GetString("logLevel")
	log.Infof("Setting log level to %s", logLevel)
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	default:
		log.Warnf("Unknown log level %s. Ignored", logLevel)
	}
}

// handlePersistentVolumeClaim queues the KukuVolume indicated the label kumori/kukuvolume
// of a PersistentVolumeClaim. The PersistentVolumeClaim is ignored if it not contains the
// kumori/kukuvolume label.
func (c *Controller) handlePersistentVolumeClaim(pvc *corev1.PersistentVolumeClaim) {
	meth := fmt.Sprintf("%s. handlePersistentVolumeClaim", c.Name)

	pvcName := pvc.GetName()
	pvcNamespace := pvc.GetNamespace()
	log.Debugf("%s. PersistentVolumeClaim: %s/%s", meth, pvcNamespace, pvcName)

	// If the PVC includes the kumori/kukuvolume label then it is a PVC generated by
	// kucontroller and must be queued
	labels := pvc.GetLabels()
	if kukuVolumeName, ok := labels[utils.KumoriKukuVolumeLabel]; ok && kukuVolumeName != "" {

		// Abort if the KukuVolume is shared or volatile
		kukuVolume, err := kvutils.GetByName(c.kukuVolumesLister, pvcNamespace, kukuVolumeName)
		if errors.IsNotFound(err) {
			log.Errorf("%s.KukuVolume '%s/%s' not found.", meth, pvcNamespace, kukuVolumeName)
			return
		}
		if err != nil {
			log.Errorf("%s. KukuVolume '%s/%s' cannot be retrieved: %v", meth, pvcNamespace, kukuVolumeName, err)
			return
		}

		// Gets cluster configuration
		clusterConfig, err := common.GetClusterConfig(c.configMapLister, &c.config)
		if err != nil {
			log.Errorf("%s. Error reading cluster configuration: %v", meth, err)
			return
		}

		// Checks if the KukuVolume is shared or volatile. Skip in that case
		isShared, err := common.IsVolumeTypeShared(kukuVolume, clusterConfig)
		if err != nil {
			log.Errorf("%s. Error checking if the KukuVolume is shared: %v", meth, err)
			return
		}
		if isShared {
			log.Debugf("%s. KukuVolume %s/%s is shared. Skipping...", meth, pvcNamespace, kukuVolumeName)
			return
		}
		isVolatile, err := common.IsVolumeTypeVolatile(kukuVolume, clusterConfig)
		if err != nil {
			log.Errorf("%s. Error checking if the KukuVolume is volatile: %v", meth, err)
			return
		}
		if isVolatile {
			log.Debugf("%s. KukuVolume %s/%s is volatile. Skipping...", meth, pvcNamespace, kukuVolumeName)
			return
		}

		// Queues the pvc
		log.Debugf("%s. Queuing kucontroller PVC '%s/%s'", meth, pvcNamespace, pvcName)
		c.EnqueueObject(pvc)
		return
	}

	// If the PVC includes the kumori/kukuvolumeitem label then it is a PVC generated by
	// this KukuVolumeItem controller ant must be queued
	if kukuVolumeItemName, ok := labels[utils.KumoriKukuVolumeItemLabel]; ok && kukuVolumeItemName != "" {
		log.Debugf("%s. Queuing temporary PVC '%s/%s'", meth, pvcNamespace, pvcName)
		c.EnqueueObject(pvc)
	}

	// Otherwise, we are not interested in this PVC
	log.Debugf("%s. PVC '%s/%s' is not of the interest of this controller. Skipping", meth, pvcNamespace, pvcName)
	return
}
