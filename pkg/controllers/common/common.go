/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package common

import (
	"encoding/json"
	"fmt"
	"strings"

	cmutils "kukuvolume-controller/pkg/utils/kube/configmaps"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	"gopkg.in/yaml.v2"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	listers "k8s.io/client-go/listers/core/v1"
)

func GetConfig(lister listers.ConfigMapLister) ControllerConfig {
	meth := "common.controller.getConfig"

	// Reads the controller manager configuration
	ns := viper.GetString("namespace")
	configMapNamespace := viper.GetString("clusterConfigMapNamespace")
	configMapName := viper.GetString("clusterConfigMapName")
	configMapKey := viper.GetString("clusterConfigMapKey")

	// Initializes the controller manager configuration
	config := ControllerConfig{
		Namespace:                 ns,
		ClusterConfigMapNamespace: configMapNamespace,
		ClusterConfigMapName:      configMapName,
		ClusterConfigMapKey:       configMapKey,
	}

	content, _ := json.MarshalIndent(config, "", "  ")
	log.Infof("%s. Configuration loaded:\n%s\n", meth, string(content))

	return config
}

// AddFinalizer accepts an Object and adds the provided finalizer if not present.
func AddFinalizer(o metav1.Object, finalizer string) {
	f := o.GetFinalizers()
	for _, e := range f {
		if e == finalizer {
			return
		}
	}
	o.SetFinalizers(append(f, finalizer))
}

// RemoveFinalizer accepts an Object and removes the provided finalizer if present.
func RemoveFinalizer(o metav1.Object, finalizer string) {
	f := o.GetFinalizers()
	for i := 0; i < len(f); i++ {
		if f[i] == finalizer {
			f = append(f[:i], f[i+1:]...)
			i--
		}
	}
	o.SetFinalizers(f)
}

// ContainsFinalizer checks an Object that the provided finalizer is present.
func ContainsFinalizer(o metav1.Object, finalizer string) bool {
	f := o.GetFinalizers()
	for _, e := range f {
		if e == finalizer {
			return true
		}
	}
	return false
}

// ErrorIsObjectUpdated returns true if the error indicates that an object update has been aborted
// due to a collision with a concurrent update which committed first
func ErrorIsObjectUpdated(err error) bool {
	return strings.Contains(err.Error(), "the object has been modified; please apply your changes to the latest version and try again")
}

// GetClusterConfig gets the ConfigMap containing the cluster configuration
func GetClusterConfig(
	lister listers.ConfigMapLister,
	config *ControllerConfig,
) (*ClusterConfig, error) {
	namespace := config.ClusterConfigMapNamespace
	name := config.ClusterConfigMapName
	key := config.ClusterConfigMapKey
	meth := fmt.Sprintf("common.controller.getClusterConfig. Namespace: '%s'. Name: '%s'. Key: '%s'", namespace, name, key)
	log.Debugf(meth)

	configMap, err := cmutils.GetByName(lister, namespace, name)
	if err != nil {
		if errors.IsNotFound(err) {
			return nil, fmt.Errorf("ConfigMap '%s' not found", name)
		}
		return nil, err
	}

	clusterConfigRaw, ok := configMap.Data[key]
	if !ok {
		return nil, fmt.Errorf("key '%s' not found in ConfigMap %s", key, name)
	}

	var clusterConfig ClusterConfig
	if err := yaml.Unmarshal([]byte(clusterConfigRaw), &clusterConfig); err != nil {
		return nil, err
	}

	generated, _ := json.MarshalIndent(clusterConfig, "", "  ")
	log.Debugf("%s. Cluster configuration:\n%s\n", meth, string(generated))

	return &clusterConfig, nil
}

func IsVolumeTypeShared(kukuVolume *kumoriv1.KukuVolume, clusterConfig *ClusterConfig) (bool, error) {
	// Gets the configuration for the kukuvolume type
	kukuVolumeType := kukuVolume.Spec.Type
	volumeTypeInfo, ok := (*clusterConfig.VolumeTypes)[kukuVolumeType]
	if !ok {
		return false, fmt.Errorf("volume type '%s' not found in cluster configuration", kukuVolumeType)
	}

	// Checks if it is a shared volume
	volumeTypeProperties := strings.Split(volumeTypeInfo.Properties, ",")
	for _, property := range volumeTypeProperties {
		if property == VolumeTypeShared {
			return true, nil
		}
	}

	return false, nil

}

func IsVolumeTypeVolatile(kukuVolume *kumoriv1.KukuVolume, clusterConfig *ClusterConfig) (bool, error) {
	// Gets the configuration for the kukuvolume type
	kukuVolumeType := kukuVolume.Spec.Type
	volumeTypeInfo, ok := (*clusterConfig.VolumeTypes)[kukuVolumeType]
	if !ok {
		return false, fmt.Errorf("volume type '%s' not found in cluster configuration", kukuVolumeType)
	}

	// Checks if it is a shared volume
	volumeTypeProperties := strings.Split(volumeTypeInfo.Properties, ",")
	for _, property := range volumeTypeProperties {
		if property == VolumeTypeVolatile {
			return true, nil
		}
	}

	return false, nil

}
