/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package common

// KukuVolumeKind is the kind of a KukuVolume
const KukuVolumeKind = "KukuVolume"

// KukuVolumeItemKind is the kind of a KukuVolume
const KukuVolumeItemKind = "KukuVolumeItem"

const (
	// UpdatedReason represents an event updating an object
	UpdatedReason string = "Updated"
	// CreatedReason represents an event creating an object
	CreatedReason string = "Created"
	// DeletedReason represents an event deleting an object
	DeletedReason string = "Deleted"
	// RequeuedReason represents an event deleting an object
	RequeuedReason string = "Requeued"
	// AbortedReason represents an event deleting an object
	AbortedReason string = "Aborted"
)

const (
	VolumeTypePersistent = "persistent"
	VolumeTypeVolatile   = "volatile"
	VolumeTypeShared     = "shared"
)
