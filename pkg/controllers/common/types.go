/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package common

// ControllerConfig contains the controller configuration parameters
type ControllerConfig struct {
	Namespace                 string `json:"namespace"`
	ClusterConfigMapName      string `json:"configMapName"`
	ClusterConfigMapNamespace string `json:"configMapNamespace"`
	ClusterConfigMapKey       string `json:"configMapKey"`
}

// ClusterConfig contains the configuration of the cluster running this kucontroller
type ClusterConfig struct {
	ClusterVersion             string                              `json:"clusterVersion" yaml:"clusterVersion"`
	ClusterName                string                              `json:"clusterName" yaml:"clusterName"`
	ReferenceDomain            string                              `json:"referenceDomain" yaml:"referenceDomain"`
	IngressDomain              string                              `json:"ingressDomain" yaml:"ingressDomain"`
	AdmissionDomain            string                              `json:"admissionDomain" yaml:"admissionDomain"`
	KumoriCtlSupportedVersions []string                            `json:"kumorictlSupportedVersions" yaml:"kumorictlSupportedVersions"`
	KumoriMgrSupportedVersions []string                            `json:"kumorimgrSupportedVersions" yaml:"kumorimgrSupportedVersions"`
	VolumeTypes                *map[string]ClusterConfigVolumeType `json:"volumeTypes,omitempty" yaml:"volumeTypes,omitempty"`
}

// ClusterConfigVolumeType contains the configuration of a volume type as provided in the cluster configuration
type ClusterConfigVolumeType struct {
	StorageClass string `json:"storageClass" yaml:"storageClass"`
	Properties   string `json:"properties" yaml:"properties"`
}
