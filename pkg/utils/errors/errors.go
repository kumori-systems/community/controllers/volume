/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package utils

import (
	"fmt"
)

const (
	// Abort error means that the Kuku-Element processing cannot be ended
	// and cannot be requeued
	Abort = "abort"
	// Retry error means that the Kuku-Element processing cannot be ended
	// but can be requeued
	Retry = "retry"
	// Wait error means that the Kuku-Element processing can continue but
	// must be requeued because there might be some pieces missing
	Wait = "wait"
)

// Text related to a KukuError created from an invalid parent
const (
	InvalidSource = "Error constructed with invalid parent"
)

// KukuErrorSeverity is the severity of a KukuError
type KukuErrorSeverity string

// KukuError is an error produced while a Kuku was created. The
// Message represents the error and the Severity the impact on the Kuku-Element
// processing.
type KukuError struct {
	Parent   error
	Severity KukuErrorSeverity
}

// Error message returns a string indicating the error severity and the parent
// error message.
func (e *KukuError) Error() string {
	return fmt.Sprintf("Severity: %s. Parent: %s", e.Severity, e.Parent.Error())
}

// NewKukuError returns a new KukuError object
func NewKukuError(severity KukuErrorSeverity, source interface{}) *KukuError {
	var parent error
	switch source.(type) {
	case error:
		parent = source.(error)
	case string:
		parent = fmt.Errorf(source.(string))
	default:
		parent = fmt.Errorf(InvalidSource)
	}
	return &KukuError{Severity: severity, Parent: parent}
}

// CheckError analyze the error and returns (ShouldAbort, ShouldRetry bool):
//   - ShouldAbort = true if is a KukuError with severity = Fatal / Major,
//     or isnt a KukuError
//   - ShouldRetry = true if is a kukuError with severity = Major / Minor
//     or isnt a KukuError
func CheckError(err error) (bool, bool) {
	switch err.(type) {
	case nil:
		return false, false
	case *KukuError:
		kerr := err.(*KukuError)
		switch kerr.Severity {
		case Abort:
			return true, false
		case Retry:
			return true, true
		case Wait:
			return false, false
		}
	}
	return true, true
}

// CheckErrorEx analyze the error, like CheckError, but in more suitable
// format for the controller flow. Returns if flow should be aborted and the
// error to be used in this case
func CheckErrorEx(err error) (bool, error) {
	shouldAbort, shouldRetry := CheckError(err)
	if shouldAbort {
		if shouldRetry {
			return true, err
		}
		return true, nil
	}
	return false, nil
}
