/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package pods

import (
	"fmt"

	"github.com/spf13/viper"
	v1 "k8s.io/api/core/v1"
	listers "k8s.io/client-go/listers/core/v1"
)

// Get recovers from the Kumori cluster the current version of a PersistentVolume, if any
func Get(
	lister listers.PodLister,
	pod *v1.Pod,
) (*v1.Pod, error) {
	if pod == nil {
		return nil, fmt.Errorf("Pod is nil")
	}
	ns := viper.GetString("namespace")
	return lister.Pods(ns).Get(pod.GetName())
}

// GetByName using the object domain and name
func GetByName(
	lister listers.PodLister,
	name string,
) (*v1.Pod, error) {

	ns := viper.GetString("namespace")

	pod, err := lister.Pods(ns).Get(name)
	if (err != nil) || (pod == nil) {
		return nil, err
	}
	return pod.DeepCopy(), nil
}
