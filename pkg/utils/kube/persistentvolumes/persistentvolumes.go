/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package persistentvolumes

import (
	"context"
	"fmt"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	"k8s.io/apimachinery/pkg/api/errors"

	utils "kukuvolume-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/core/v1"
)

// Create is used to create a new persistentVolume in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	persistentVolume *v1.PersistentVolume,
) (*v1.PersistentVolume, error) {
	if persistentVolume == nil {
		return nil, fmt.Errorf("PersistentVolume is nil")
	}
	log.Debugf("persistentVolumes.Create. PersistentVolume: %s", persistentVolume.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kukuvolume-controller",
	}
	return client.CoreV1().PersistentVolumes().Create(ctx, persistentVolume, options)
}

// Update is used to update an existing persistentVolume in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	persistentVolume *v1.PersistentVolume,
) (*v1.PersistentVolume, error) {
	if persistentVolume == nil {
		return nil, fmt.Errorf("PersistentVolume is nil")
	}
	log.Debugf("persistentVolumes.Update. PersistentVolume: %s", persistentVolume.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kukuvolume-controller",
	}
	return client.CoreV1().PersistentVolumes().Update(ctx, persistentVolume, options)
}

// Delete is used to delete an existing persistentVolume in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	persistentVolume *v1.PersistentVolume,
) error {
	if persistentVolume == nil {
		return fmt.Errorf("PersistentVolume is nil")
	}
	log.Debugf("persistentVolumes.Delete. PersistentVolume: %s", persistentVolume.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.CoreV1().PersistentVolumes().Delete(ctx, persistentVolume.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a PersistentVolume, if any
func Get(
	lister listers.PersistentVolumeLister,
	persistentVolume *v1.PersistentVolume,
) (*v1.PersistentVolume, error) {
	if persistentVolume == nil {
		return nil, fmt.Errorf("PersistentVolume is nil")
	}
	return lister.Get(persistentVolume.GetName())
}

// GetByName using the object domain and name
func GetByName(
	lister listers.PersistentVolumeLister,
	name string,
) (*v1.PersistentVolume, error) {
	pv, err := lister.Get(name)
	if (err != nil) || (pv == nil) {
		return nil, err
	}
	return pv.DeepCopy(), nil
}

// CreateOrUpdate checks if the given persistentVolume exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.PersistentVolumeLister,
	persistentVolume *v1.PersistentVolume,
) (*v1.PersistentVolume, utils.Operation, error) {
	if persistentVolume == nil {
		return nil, utils.NoneOperation, fmt.Errorf("PersistentVolume is nil")
	}
	currentPersistentVolume, err := Get(lister, persistentVolume)
	if errors.IsNotFound(err) {
		finalPersistentVolume, err := Create(client, persistentVolume)
		return finalPersistentVolume, utils.CreatedOperation, err
	} else if err != nil {
		return persistentVolume, utils.NoneOperation, err
	}
	currentPersistentVolume = currentPersistentVolume.DeepCopy()
	finalPersistentVolume, changed, err := Apply(client, persistentVolume, currentPersistentVolume)
	if changed {
		return finalPersistentVolume, utils.UpdatedOperation, err
	}

	return finalPersistentVolume, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of persistentVolumes in the cluster. Returns
// the list of created persistentVolumes, the list of updated persistentVolumes and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.PersistentVolumeLister,
	persistentVolumeList []v1.PersistentVolume,
) (created []v1.PersistentVolume, updated []v1.PersistentVolume, errors []error) {
	listLength := len(persistentVolumeList)
	errors = make([]error, 0, 3)
	created = make([]v1.PersistentVolume, 0, listLength)
	updated = make([]v1.PersistentVolume, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, persistentVolume := range persistentVolumeList {
		newPersistentVolume, operation, err := CreateOrUpdate(client, lister, &persistentVolume)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newPersistentVolume)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newPersistentVolume)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for persistentVolume %s. Expected operations are %s or %s", operation, persistentVolume.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of persistentVolumes and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.PersistentVolumeLister,
	persistentVolumesList []v1.PersistentVolume,
	selector labels.Selector,
) (
	created []v1.PersistentVolume,
	updated []v1.PersistentVolume,
	deleted []v1.PersistentVolume,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, persistentVolumesList)
	currentPersistentVolumesList, err := lister.List(selector)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.PersistentVolume{}
		return
	}
	if (currentPersistentVolumesList != nil) && (len(currentPersistentVolumesList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentPersistentVolumesList, persistentVolumesList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.PersistentVolume{}
	}
	return
}

// Apply updates "to" persistentVolume with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.PersistentVolume,
	to *v1.PersistentVolume,
) (*v1.PersistentVolume, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From persistentVolume is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("To persistentVolume is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("persistentVolumes.Apply. Owner references differ for %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range (*from).GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("persistentVolumes.Apply. Labels differ in %s", from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	for key, newValue := range (*from).GetAnnotations() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("persistentVolumes.Apply. Annotations differ in %s", from.GetName())
			changed = true
			to.Annotations[key] = newValue
		}
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.ClaimRef, to.Spec.ClaimRef) {
		log.Debugf("persistentVolumes.Apply. ClaimRef differ in %s", from.GetName())
		changed = true
		to.Spec.ClaimRef = from.Spec.ClaimRef
	}

	if changed {
		newPersistentVolume, err := Update(client, to)
		return newPersistentVolume, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a persistentVolume in a cluster if cannot be found in a list of persistentVolumes.
// Returns true if the persistentVolume has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	persistentVolume *v1.PersistentVolume,
	expectedList []v1.PersistentVolume,
) (bool, error) {
	if persistentVolume == nil {
		return false, fmt.Errorf("PersistentVolume is nil")
	}
	for _, expectedPersistentVolume := range expectedList {
		if (persistentVolume.GetNamespace() == expectedPersistentVolume.GetNamespace()) &&
			(persistentVolume.GetName() == expectedPersistentVolume.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, persistentVolume); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all PersistentVolumes not found in existingList.
// A PersistentVolumeis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.PersistentVolume,
	expectedList []v1.PersistentVolume,
) (deleted []v1.PersistentVolume, errors []error) {
	deleted = make([]v1.PersistentVolume, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}
