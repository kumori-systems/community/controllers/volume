/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package persistentvolumeclaims

import (
	"context"
	"fmt"

	apiequality "k8s.io/apimachinery/pkg/api/equality"

	"k8s.io/apimachinery/pkg/api/errors"

	utils "kukuvolume-controller/pkg/utils"

	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/core/v1"
)

// Create is used to create a new persistentVolumeClaim in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	persistentVolumeClaim *v1.PersistentVolumeClaim,
) (*v1.PersistentVolumeClaim, error) {
	if persistentVolumeClaim == nil {
		return nil, fmt.Errorf("PersistentVolumeClaim is nil")
	}
	log.Debugf("persistentVolumeClaims.Create. PersistentVolumeClaim: %s", persistentVolumeClaim.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kukuvolume-controller",
	}
	return client.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Create(ctx, persistentVolumeClaim, options)
}

// Update is used to update an existing persistentVolumeClaim in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	persistentVolumeClaim *v1.PersistentVolumeClaim,
) (*v1.PersistentVolumeClaim, error) {
	if persistentVolumeClaim == nil {
		return nil, fmt.Errorf("PersistentVolumeClaim is nil")
	}
	log.Debugf("persistentVolumeClaims.Update. PersistentVolumeClaim: %s", persistentVolumeClaim.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kukuvolume-controller",
	}
	return client.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Update(ctx, persistentVolumeClaim, options)
}

// Delete is used to delete an existing persistentVolumeClaim in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	persistentVolumeClaim *v1.PersistentVolumeClaim,
) error {
	if persistentVolumeClaim == nil {
		return fmt.Errorf("PersistentVolumeClaim is nil")
	}
	log.Debugf("persistentVolumeClaims.Delete. PersistentVolumeClaim: %s", persistentVolumeClaim.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.CoreV1().PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Delete(ctx, persistentVolumeClaim.GetName(), options)
}

// Get recovers from the Kumori cluster the current version of a PersistentVolumeClaim, if any
func Get(
	lister listers.PersistentVolumeClaimLister,
	persistentVolumeClaim *v1.PersistentVolumeClaim,
) (*v1.PersistentVolumeClaim, error) {
	if persistentVolumeClaim == nil {
		return nil, fmt.Errorf("PersistentVolumeClaim is nil")
	}
	return lister.PersistentVolumeClaims(persistentVolumeClaim.GetNamespace()).Get(persistentVolumeClaim.GetName())
}

// GetByName using the object domain and name
func GetByName(
	lister listers.PersistentVolumeClaimLister,
	namespace string,
	name string,
) (*v1.PersistentVolumeClaim, error) {
	pvc, err := lister.PersistentVolumeClaims(namespace).Get(name)
	if (err != nil) || (pvc == nil) {
		return nil, err
	}
	return pvc.DeepCopy(), nil
}

// CreateOrUpdate checks if the given persistentVolumeClaim exists in the cluster and updates it
// if exists or creates it otherwirse. Returns also a flag indicating if the object has
// been created (true) or updated (false).
func CreateOrUpdate(
	client clientset.Interface,
	lister listers.PersistentVolumeClaimLister,
	persistentVolumeClaim *v1.PersistentVolumeClaim,
) (*v1.PersistentVolumeClaim, utils.Operation, error) {
	if persistentVolumeClaim == nil {
		return nil, utils.NoneOperation, fmt.Errorf("PersistentVolumeClaim is nil")
	}
	currentPersistentVolumeClaim, err := Get(lister, persistentVolumeClaim)
	if errors.IsNotFound(err) {
		finalPersistentVolumeClaim, err := Create(client, persistentVolumeClaim)
		return finalPersistentVolumeClaim, utils.CreatedOperation, err
	} else if err != nil {
		return persistentVolumeClaim, utils.NoneOperation, err
	}
	currentPersistentVolumeClaim = currentPersistentVolumeClaim.DeepCopy()
	finalPersistentVolumeClaim, changed, err := Apply(client, persistentVolumeClaim, currentPersistentVolumeClaim)
	if changed {
		return finalPersistentVolumeClaim, utils.UpdatedOperation, err
	}

	return finalPersistentVolumeClaim, utils.NoneOperation, err
}

// CreateOrUpdateList creates or updates a list of persistentVolumeClaims in the cluster. Returns
// the list of created persistentVolumeClaims, the list of updated persistentVolumeClaims and the list of errors
// found during the process.
func CreateOrUpdateList(
	client clientset.Interface,
	lister listers.PersistentVolumeClaimLister,
	persistentVolumeClaimList []v1.PersistentVolumeClaim,
) (created []v1.PersistentVolumeClaim, updated []v1.PersistentVolumeClaim, errors []error) {
	listLength := len(persistentVolumeClaimList)
	errors = make([]error, 0, 3)
	created = make([]v1.PersistentVolumeClaim, 0, listLength)
	updated = make([]v1.PersistentVolumeClaim, 0, listLength)
	if listLength <= 0 {
		return
	}
	for _, persistentVolumeClaim := range persistentVolumeClaimList {
		newPersistentVolumeClaim, operation, err := CreateOrUpdate(client, lister, &persistentVolumeClaim)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if operation == utils.NoneOperation {
			continue
		}
		if operation == utils.CreatedOperation {
			created = append(created, *newPersistentVolumeClaim)
			continue
		}
		if operation == utils.UpdatedOperation {
			updated = append(updated, *newPersistentVolumeClaim)
			continue
		}

		errors = append(errors, fmt.Errorf("Unexpected applied operation %s for PersistentVolumeClaim %s. Expected operations are %s or %s", operation, persistentVolumeClaim.GetName(), utils.CreatedOperation, utils.UpdatedOperation))
	}
	return
}

// CreateUpdateOrDeleteList receives a list of persistentVolumeClaims and creates all elements in that
// list not currently part of the cluster state, updates those which are part of the cluster
// state but are outdated and deletes those currently existing in the cluster state but not
// in the list
func CreateUpdateOrDeleteList(
	client clientset.Interface,
	lister listers.PersistentVolumeClaimLister,
	persistentVolumeClaimsList []v1.PersistentVolumeClaim,
	selector labels.Selector,
	namespace string,
) (
	created []v1.PersistentVolumeClaim,
	updated []v1.PersistentVolumeClaim,
	deleted []v1.PersistentVolumeClaim,
	errors []error,
) {
	created, updated, errors = CreateOrUpdateList(client, lister, persistentVolumeClaimsList)
	currentPersistentVolumeClaimsList, err := lister.PersistentVolumeClaims(namespace).List(selector)
	if err != nil {
		errors = append(errors, err)
		deleted = []v1.PersistentVolumeClaim{}
		return
	}
	if (currentPersistentVolumeClaimsList != nil) && (len(currentPersistentVolumeClaimsList) > 0) {
		var errs []error
		deleted, errs = DeleteMissing(client, currentPersistentVolumeClaimsList, persistentVolumeClaimsList)
		if (errs != nil) && (len(errs) > 0) {
			errors = append(errors, errs...)
		}
	} else {
		deleted = []v1.PersistentVolumeClaim{}
	}
	return
}

// Apply updates "to" persistentVolumeClaim with "from" values if any change is detected. The changes
// are also applied to a kubernetes cluster
func Apply(
	client clientset.Interface,
	from *v1.PersistentVolumeClaim,
	to *v1.PersistentVolumeClaim,
) (*v1.PersistentVolumeClaim, bool, error) {

	if from == nil {
		return to, false, fmt.Errorf("From PersistentVolumeClaim is null")
	}

	if to == nil {
		return to, false, fmt.Errorf("To PersistentVolumeClaim is null")
	}

	changed := false

	if !apiequality.Semantic.DeepEqual(from.OwnerReferences, to.OwnerReferences) {
		log.Debugf("PersistentVolumeClaims.Apply. Owner references differ for %s", from.GetName())
		changed = true
		to.OwnerReferences = from.OwnerReferences
	}

	for key, newValue := range (*from).GetLabels() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("PersistentVolumeClaims.Apply. Labels differ in %s", from.GetName())
			changed = true
			to.Labels[key] = newValue
		}
	}

	for key, newValue := range (*from).GetAnnotations() {
		if currentValue, ok := to.Labels[key]; !ok || (currentValue != newValue) {
			log.Debugf("PersistentVolumeClaims.Apply. Annotations differ in %s", from.GetName())
			changed = true
			to.Annotations[key] = newValue
		}
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.StorageClassName, to.Spec.StorageClassName) {
		log.Debugf("PersistentVolumeClaims.Apply. StorageClassName differ in %s", from.GetName())
		changed = true
		to.Spec.StorageClassName = from.Spec.StorageClassName
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.VolumeMode, to.Spec.VolumeMode) {
		log.Debugf("PersistentVolumeClaims.Apply. VolumeMode differ in %s", from.GetName())
		changed = true
		to.Spec.VolumeMode = from.Spec.VolumeMode
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.AccessModes, to.Spec.AccessModes) {
		log.Debugf("PersistentVolumeClaims.Apply. AccessModes differ in %s", from.GetName())
		changed = true
		to.Spec.AccessModes = from.Spec.AccessModes
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Selector, to.Spec.Selector) {
		log.Debugf("PersistentVolumeClaims.Apply. Selector differ in %s", from.GetName())
		changed = true
		to.Spec.AccessModes = from.Spec.AccessModes
	}

	if !apiequality.Semantic.DeepEqual(from.Spec.Resources, to.Spec.Resources) {
		log.Debugf("PersistentVolumeClaims.Apply. Resources differ in %s", from.GetName())
		changed = true
		to.Spec.AccessModes = from.Spec.AccessModes
	}

	if changed {
		newPersistentVolumeClaim, err := Update(client, to)
		return newPersistentVolumeClaim, changed, err
	}

	return to, false, nil
}

// DeleteIfMissing deletes a persistentVolumeClaim in a cluster if cannot be found in a list of persistentVolumeClaims.
// Returns true if the persistentVolumeClaim has been deleted and false otherwise
func DeleteIfMissing(
	client clientset.Interface,
	persistentVolumeClaim *v1.PersistentVolumeClaim,
	expectedList []v1.PersistentVolumeClaim,
) (bool, error) {
	if persistentVolumeClaim == nil {
		return false, fmt.Errorf("PersistentVolumeClaim is nil")
	}
	for _, expectedPersistentVolumeClaim := range expectedList {
		if (persistentVolumeClaim.GetNamespace() == expectedPersistentVolumeClaim.GetNamespace()) &&
			(persistentVolumeClaim.GetName() == expectedPersistentVolumeClaim.GetName()) {
			return false, nil
		}
	}
	if err := Delete(client, persistentVolumeClaim); err != nil {
		return false, err
	}
	return true, nil
}

// DeleteMissing deletes from currentList all PersistentVolumeClaims not found in existingList.
// A PersistentVolumeClaimis in existingList if there is an element in that list with the same name
// and namespace.
func DeleteMissing(
	client clientset.Interface,
	currentList []*v1.PersistentVolumeClaim,
	expectedList []v1.PersistentVolumeClaim,
) (deleted []v1.PersistentVolumeClaim, errors []error) {
	deleted = make([]v1.PersistentVolumeClaim, 0, len(currentList))
	errors = make([]error, 0, len(currentList))
	for _, current := range currentList {
		if current == nil {
			continue
		}
		del, err := DeleteIfMissing(client, current, expectedList)
		if err != nil {
			errors = append(errors, err)
			continue
		}
		if del {
			deleted = append(deleted, *current)
		}
	}
	return deleted, errors
}
