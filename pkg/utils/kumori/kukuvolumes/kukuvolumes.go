/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumes

import (
	"context"
	"fmt"
	"kukuvolume-controller/pkg/controllers/common"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	clientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Get recovers from the Kumori cluster the current version of a KukuVolume, if any
func Get(
	lister kumorilisters.KukuVolumeLister,
	volume *kumoriv1.KukuVolume,
) (*kumoriv1.KukuVolume, error) {
	if volume == nil {
		return nil, fmt.Errorf("KukuVolume is nil")
	}
	return GetByName(lister, volume.GetNamespace(), volume.GetName())
}

// GetByName using the object domain and name
func GetByName(
	lister kumorilisters.KukuVolumeLister,
	namespace string,
	name string,
) (*kumoriv1.KukuVolume, error) {
	kukuVolume, err := lister.KukuVolumes(namespace).Get(name)
	if err != nil {
		return nil, err
	}
	if kukuVolume == nil {
		return nil, nil
	}
	return kukuVolume.DeepCopy(), nil
}

// UpdateStatus is used to update an existing KukuVolume in a Kubernetes cluster using a client-go
// instance.
func UpdateStatus(
	client clientset.Interface,
	kukuVolume *kumoriv1.KukuVolume,
) (*kumoriv1.KukuVolume, error) {
	if kukuVolume == nil {
		return nil, fmt.Errorf("KukuVolume is nil")
	}
	log.Debugf("kukuVolume.UpdateStatus. KukuVolume: %s", kukuVolume.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kukuvolume-controller",
	}
	kukuVolume.TypeMeta = metav1.TypeMeta{
		Kind:       common.KukuVolumeKind,
		APIVersion: "kumori.systems/v1",
	}
	return client.KumoriV1().KukuVolumes(kukuVolume.Namespace).UpdateStatus(ctx, kukuVolume, options)
}
