/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package kukuvolumeitems

import (
	"context"
	"fmt"
	"kukuvolume-controller/pkg/controllers/common"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	clientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
)

// Get recovers from the Kumori cluster the current version of a KukuVolumeItem, if any
func Get(
	lister kumorilisters.KukuVolumeItemLister,
	volume *kumoriv1.KukuVolumeItem,
) (*kumoriv1.KukuVolumeItem, error) {
	if volume == nil {
		return nil, fmt.Errorf("KukuVolumeItem is nil")
	}
	return GetByName(lister, volume.GetNamespace(), volume.GetName())
}

// GetByName using the object domain and name
func GetByName(
	lister kumorilisters.KukuVolumeItemLister,
	namespace string,
	name string,
) (*kumoriv1.KukuVolumeItem, error) {
	kvi, err := lister.KukuVolumeItems(namespace).Get(name)
	if (err != nil) || (kvi == nil) {
		return nil, err
	}
	return kvi.DeepCopy(), nil
}

// List returns a list of KukuVolumeItems fulfilling a given selector
func List(
	lister kumorilisters.KukuVolumeItemLister,
	namespace string,
	selector labels.Selector,
) ([]*kumoriv1.KukuVolumeItem, error) {
	kukuVolumeItems, err := lister.KukuVolumeItems(namespace).List(selector)

	if err != nil {
		return nil, err
	}

	return kukuVolumeItems, nil
}

// Create is used to create a new kukuVolumeItem in a Kubernetes cluster using a client-go
// instance.
func Create(
	client clientset.Interface,
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) (*kumoriv1.KukuVolumeItem, error) {
	if kukuVolumeItem == nil {
		return nil, fmt.Errorf("KukuVolumeItem is nil")
	}
	log.Debugf("kukuVolumeItems.Create. KukuVolumeItem: %s", kukuVolumeItem.Name)
	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kukuvolume-controller",
	}
	return client.KumoriV1().KukuVolumeItems(kukuVolumeItem.GetNamespace()).Create(ctx, kukuVolumeItem, options)
}

// Update is used to update an existing kukuVolumeItem in a Kubernetes cluster using a client-go
// instance.
func Update(
	client clientset.Interface,
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) (*kumoriv1.KukuVolumeItem, error) {
	if kukuVolumeItem == nil {
		return nil, fmt.Errorf("KukuVolumeItem is nil")
	}
	log.Debugf("kukuVolumeItems.Update. KukuVolumeItem: %s", kukuVolumeItem.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kukuvolume-controller",
	}
	return client.KumoriV1().KukuVolumeItems(kukuVolumeItem.GetNamespace()).Update(ctx, kukuVolumeItem, options)
}

// UpdateStatus is used to update an existing KukuVolumeItem in a Kubernetes cluster using a client-go
// instance.
func UpdateStatus(
	client clientset.Interface,
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) (*kumoriv1.KukuVolumeItem, error) {
	if kukuVolumeItem == nil {
		return nil, fmt.Errorf("KukuVolumeItem is nil")
	}
	log.Debugf("kukuVolumeItem.UpdateStatus. KukuVolumeItem: %s", kukuVolumeItem.Name)
	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kukuvolume-controller",
	}
	kukuVolumeItem.TypeMeta = metav1.TypeMeta{
		Kind:       common.KukuVolumeItemKind,
		APIVersion: "kumori.systems/v1",
	}
	return client.KumoriV1().KukuVolumeItems(kukuVolumeItem.Namespace).UpdateStatus(ctx, kukuVolumeItem, options)
}

// Delete is used to delete an existing kukuVolumeItem in a Kubernetes cluster using a client-go
// instance.
func Delete(
	client clientset.Interface,
	kukuVolumeItem *kumoriv1.KukuVolumeItem,
) error {
	if kukuVolumeItem == nil {
		return fmt.Errorf("KukuVolumeItem is nil")
	}
	log.Debugf("kukuVolumeItems.Delete. KukuVolumeItem: %s", kukuVolumeItem.Name)
	ctx := context.TODO()
	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	return client.KumoriV1().KukuVolumeItems(kukuVolumeItem.GetNamespace()).Delete(ctx, kukuVolumeItem.GetName(), options)
}
