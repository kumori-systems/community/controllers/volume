/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"os/user"
	"path"
	"path/filepath"
	"time"

	kumoriclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions"

	kukuvolume "kukuvolume-controller/pkg/controllers/kukuvolume"
	kukuvolumeshared "kukuvolume-controller/pkg/controllers/kukuvolume-shared"
	kukuvolumeitem "kukuvolume-controller/pkg/controllers/kukuvolumeitem"

	stopsignal "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/stop-signal"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	kubeinformers "k8s.io/client-go/informers"
	kubeclientset "k8s.io/client-go/kubernetes"
	rest "k8s.io/client-go/rest"
	clientcmd "k8s.io/client-go/tools/clientcmd"
)

// How many workers will be launched
const threadiness int = 1

// Command-line args (see init function)
var ns string
var labelselector string
var kubeconfig string
var resyncInterval time.Duration

// init reads command-line args (init function is executed when package is imported)
func init() {
	meth := "ControllerManager.init"
	var writers io.Writer
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		fmt.Println("main.init. Error getting kukuvolume-controller path: ", err)
		writers = io.MultiWriter(os.Stdout)
	} else {
		logfilepath := path.Join(dir, "kukuvolume-controller.log")
		file, err := os.OpenFile(logfilepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err != nil {
			fmt.Println("main.init. Error creating log file: ", err)
			writers = io.MultiWriter(os.Stdout)
		} else {
			writers = io.MultiWriter(os.Stdout, file)
		}
	}
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	})
	log.SetOutput(writers)
	log.Debug("main.init")
	flag.StringVar(&ns, "namespace", "",
		"Namespace to take into acount")
	flag.StringVar(&labelselector, "label", "",
		"'LabelName=LabelValue' to take into acount. All, if not specified")
	flag.StringVar(&kubeconfig, "kubeconfig", "",
		"Paths to a kubeconfig. Defaults, in this order, to KUBECONFIG environment variable, to in cluser credentials or to $HOME/.kube/config otherwise")
	flag.DurationVar(&resyncInterval, "resync", 0,
		"Resync interval (5s, 1m...). Never, if not specified or 0")
	flag.String("logLevel", "debug",
		"Minimum severity to show a log line: debug, info (default), warn, err")
	flag.String("clusterConfigMapName", "cluster-config", "Name of the configmap storing the cluster configuration")
	flag.String("clusterConfigMapNamespace", "kumori", "Namespace of the configmap storing the cluster configuration")
	flag.String("clusterConfigMapKey", "cluster-config.yaml", "Key storing the cluster configuration in the ConfigMap described in clusterConfigMapName property")

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/kumori")
	viper.BindPFlags(pflag.CommandLine)

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Warnf("%s. Config file '/kumori/config.json' not found", meth)
		} else {
			log.Warnf("%s. Error reading configuration file in '/kumori/config.json': %s", meth, err.Error())
		}
	}

	kukuvolume.UpdateLogLevel()
	kukuvolumeitem.UpdateLogLevel()
	kukuvolumeshared.UpdateLogLevel()

	ns = viper.GetString("namespace")
	labelselector = viper.GetString("label")
	kubeconfig = viper.GetString("kubeconfig")
	resyncInterval = viper.GetDuration("resync")
}

// getConfig loads a REST Config, depending of the environment and flags:
// Config precedence:
// * --kubeconfig flag pointing at a file
// * KUBECONFIG environment variable pointing at a file
// * In-cluster config if running in cluster
// * $HOME/.kube/config if exists (default location in the user's home directory)
func getConfig() (config *rest.Config, err error) {
	log.Debug("main.getConfig")
	if len(kubeconfig) > 0 {
		log.Debug("main.getConfig. Kubeconfig flag found: ", kubeconfig)
		return clientcmd.BuildConfigFromFlags("", kubeconfig)
	}
	if len(os.Getenv("KUBECONFIG")) > 0 {
		log.Debug("main.getConfig. KUBECONFIG environment variable found: ", os.Getenv("KUBECONFIG"))
		return clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
	}
	if c, err := rest.InClusterConfig(); err == nil {
		fmt.Println("main.getConfig. In cluster config")
		return c, nil
	}
	if usr, err := user.Current(); err == nil {
		c, err := clientcmd.BuildConfigFromFlags("", filepath.Join(usr.HomeDir, ".kube", "config"))
		if err == nil {
			return c, nil
		}
	}
	return nil, fmt.Errorf("could not locate a kubeconfig")
}

// setQPSBurst applies saner defaults for QPS and burst based on the Kubernetes
// controller manager defaults (20 QPS, 30 burst)
// These 2 flags set normal and burst rate that controller can talk to
// kube-apiserver.
// Default values work pretty well (20 for QPS and 30 for burst).
// Increase these values for large, production clusters.
func setQPSBurst(config *rest.Config) {
	if config.QPS == 0.0 {
		config.QPS = 20.0
		config.Burst = 30.0
	}
}

// main is the controller entrypoint
func main() {
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: true,
	})
	log.SetLevel(log.DebugLevel)
	log.Info("main.main() Running kukuvolume-controller...")

	// Set up signals so we handle the shutdown signal gracefully
	stopCh := stopsignal.SetupStopSignal()
	errorCh := make(chan error)

	flag.Parse()
	if ns == "" {
		log.Fatal("main.main() Namespace command-line arg is mandatory")
	}

	config, err := getConfig()
	if err != nil {
		log.Fatal("main.main() Error getting Kubernetes config: ", err)
	}
	setQPSBurst(config)

	// Create a clientset and informer-factory for kubernetes standard objects
	// - Clientset is built as an abstraction above a RESTClient, and exposes
	//   versioned API resources and their serializers.
	//   Clientset contains the clients for groups, and each group has exactly
	//   one version included in a Clientset.
	// - SharedInformer provfmt.Printlnides eventually consistent linkage of its
	//   clients to the authfmt.Printlnoritative state of a given collection of
	//   objects.  An object is identified by its API group, kind/resource,
	//   namespace, and name.  One SharedInfomer provides linkage to objects
	//   of a particular API group and kind/resource.  The linked object
	//   collection of a SharedInformer may be further restricted to one
	//   namespace and/or by label selector and/or field selector.
	//   Informers will “sync” periodically`(resyncInterval parameter), they will
	//   deliver every matching object in the cluster to your Update method.
	//   This is good for cases where you may need to take additional action on
	//   the object, but sometimes you know there won't be more work to do.
	kubeClientset, err := kubeclientset.NewForConfig(config)
	if err != nil {
		log.Fatal("main.main() Error building kubernetes clientset: ", err)
	}
	kubeInformerFactory := kubeinformers.NewFilteredSharedInformerFactory(
		kubeClientset,
		resyncInterval,
		ns,
		func(opt *metav1.ListOptions) {
			if labelselector != "" {
				opt.LabelSelector = labelselector
			}
		},
	)

	// Create a clientset and informer-factory for KukuComponent objects
	kumoriClientset, err := kumoriclientset.NewForConfig(config)
	if err != nil {
		log.Fatal("main.main() Error creating the crd clientset: ", err)
	}
	kumoriInformerFactory := kumoriinformers.NewFilteredSharedInformerFactory(
		kumoriClientset,
		resyncInterval,
		ns,
		func(opt *metav1.ListOptions) {
			if labelselector != "" {
				opt.LabelSelector = labelselector
			}
		},
	)

	// Create the controller that manage persistent KukuVolumes
	kukuVolumeController := kukuvolume.NewController(
		kubeClientset,
		kumoriClientset,
		kubeInformerFactory.Core().V1().PersistentVolumes(),
		kubeInformerFactory.Core().V1().PersistentVolumeClaims(),
		kumoriInformerFactory.Kumori().V1().KukuVolumes(),
		kumoriInformerFactory.Kumori().V1().KukuVolumeItems(),
		kubeInformerFactory.Core().V1().Pods(),
		kubeInformerFactory.Core().V1().ConfigMaps(),
	)

	// Create the controller that manage KukuVolumeItems
	kukuVolumeItemController := kukuvolumeitem.NewController(
		kubeClientset,
		kumoriClientset,
		kubeInformerFactory.Core().V1().PersistentVolumes(),
		kubeInformerFactory.Core().V1().PersistentVolumeClaims(),
		kumoriInformerFactory.Kumori().V1().KukuVolumes(),
		kumoriInformerFactory.Kumori().V1().KukuVolumeItems(),
		kubeInformerFactory.Core().V1().ConfigMaps(),
	)

	// Create the controller that manage shared KukuVolumes
	kukuVolumeSharedController := kukuvolumeshared.NewController(
		kubeClientset,
		kumoriClientset,
		kubeInformerFactory.Core().V1().PersistentVolumeClaims(),
		kumoriInformerFactory.Kumori().V1().KukuVolumes(),
		kubeInformerFactory.Core().V1().ConfigMaps(),
	)
	// Start informers
	// Notice that there is no need to run Start methods in a separate goroutine.
	// Start method is non-blocking and runs all registered informers in a
	// dedicated goroutine.
	kubeInformerFactory.Start(stopCh)
	kumoriInformerFactory.Start(stopCh)

	// Start component controllers
	go kukuVolumeController.Run(threadiness, stopCh, errorCh)
	go kukuVolumeItemController.Run(threadiness, stopCh, errorCh)
	go kukuVolumeSharedController.Run(threadiness, stopCh, errorCh)

	select {
	case err := <-errorCh:
		log.Fatal("main.main() Error running kukuvolume-controller", err)
	case <-stopCh:
		log.Info("main.main() Closing kukuvolume-controller")
	}

	// Wait application closed
	<-stopCh
	log.Info("main.main() Closing kukuvolume-controller")
}
