= KuVolumeController
:toc:

== Introduction

The _KuVolumeController_ is in charge of preparing _PersistentVolume_ objects to back the _KukuVolume_ resources, manage their lifecycle and manage their binding to different _PersistentVolumeClaim_ objects during their lifetime.

== Usage

The Volume Controller is one of the Kumori Platform's controller managers. It has been designed to be executed as a Kubernetes _Deployment_ during a Kumori Cluster installation (see https://gitlab.com/kumori-systems/community/libraries/kumorimgr-scripts[kumorimgr-scripts]).

=== Configuration

The Volume Controller includes the following command line arguments:

[source,console]
----
$ bin/manager --help
Usage of bin/manager:
      --kubeconfig string   Paths to a kubeconfig. Defaults, in this order, to KUBECONFIG environment variable, to in cluser credentials or to $HOME/.kube/config otherwise
      --label string        'LabelName=LabelValue' to take into acount. All, if not specified
      --logLevel string     Minimum severity to show a log line: debug, info (default), warn, err (default "debug")
      --namespace string    Namespace to take into acount.
      --resync duration     Resync interval (5s, 1m...). Never, if not specified or 0. (default 0s)
----

NOTE: Only the `namespace` is mandatory.

The Volume Controller also reads the `storageClasses` key from the file stored in `/kumori/config.yaml` (if exists), which defaults to:

[source,yaml]
----
storageClasses:
  persistent: openebs-replicated
	volatile: openebs-locallvm
----

This key contains a dictionary containing the storage class per configured Kumori volume type. By default, two volume types are configured:

* `persistent`: volumes which can be assigned to role instance to store persitent data. Must be backed by a persistent storage system.
* `volatile`: ephemeral volumes used by role instances to store temporary information.


=== Development and testing

For testing purposes during development, it can be also executed as a separated process in a computer with administration credentials over a Kumori Cluster.

[source,console]
----
$ make run
....
----

NOTE: Remember to disable the Volume Controller in your cluster before executing it manually from your computer.

Log lines will be shown in the standard output and stored in `bin/kukuvolume-controller.log`.

== Design

The Volume Controller manager contains two controllers:

* _kukuvolume_ controller: stored in `pkg/controllers/deployment`. Manages _KukuVolumeItem_ objects (or _volumeetos_). A _KukuVolumeItem_ is backed by a _PersistentVolume_ and can be assigned to a role instance requiring a Kumori volume. When a _KukuVolumeItem_ is assigned to a instance _PersistentVolumeClaim_, this controller is also in charge of biinding the _PersistentVolume_ to this _PersistentVolumeClaim_.
* _kukuvolumeitem_ controller: stored in `/pkg/kukuvolumeitem`. Manages the _PersistentVolume_ objects assigned to _KukuVolumeItem_ objects.

=== KukuVolume Controller

This controller queues a _PersistentVolumeClaim_ object if:

* It has been created by the _kucontroller_ to claim a persistent volume for a role instance. In that case, the _PersistentVolumeClaim_ must has a `kumori.systems/kukuvolume` label containing the name of the _KukuVolume_ assigned to this role. Example:

[source,yaml]
----
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  labels:
    kumori.systems/kukuvolume: kv-101337-b4760ca2
    ...
  ...
...
----

* It has been created by the _kukuvolumeitem_ controller to trigger the creation of a _PersistentVolume_ for a given _KukuVolumeItem_. In this case, the _PersistentVolumeClaim_ must has a `kumori.systems/kukuvolumeitem` label containing the name of the _KukuVolumeItem_. For example:

[source,yaml]
----
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  labels:
    kumori.systems/kukuvolumeitem: kv-101337-b4760ca2-epmie
    ...
  ...
...
----

The actions taken by the controller completely differ depending on which type of _PersistentVolumeClaim_ is processing. Hence, in some sense, this controller can be considered as two controllers:

* The _kukucontroller pvc_ controller. Manages _PersistentVolumeClaim_ objects containing the `kumori.systems/kukuvolume` label.
* The _kukuvolumeitem pvc_ controller. Manages _PersistentVolumeClaim_ objects containing the `kumori.systems/kukuvolumeitem` label.

==== KukuController PVC Controller

This controller runs the following reconciliation algorithm:

. If the _PersistentVolumeClaim_ is being deleted (it has a deletion timestamp set):
.. If exists a _KukuVolumeItem_ with its `spec.persistentVolumeClaimRef` set to this _PersistentVolumeClaim_, remove this reference from the _KukuVolumeItem_.
.. If it has a `spec.volumeName` set, get the _PersistentVolume_ name with that name and set its `spec.claimRef` to a mock name and uid.
.. Remove the `kumori.systems/kukuvolume-pvc-finalizer` finalizer.
.. End (without requeuing)
. Adds the `kumori.systems/kukuvolume-pvc-finalizer` if it has not been previously added.
. If the _PersistentVolumeClaim_ is in `Bound` status.
.. End without requeuing. This _PersistentVolumeClaim_ already has a _PersistentVolume_ assigned.
. If exists a _KukuVolumeItem_ already assigned to this _PersistentVolumeClaim_:
.. End without requeuing. Eventually, a _PersistentVolume_ will be assigned to this _PersistentVolumeClaim_.
. If the _KukuVolume_ named as the `kumori.systems/kukuvolume` label value has a _KukuVolumeItem_ in _Available_ phase:
.. Assign this _KukuVolumeItem_ by setting its `spec.persistentVolumeClaimRef` to the name of the _PersistentVolumeClaim_.
.. Get the _PersistentVolume_ assigned to this _KukuVolumeItem_ and set its `spec.claimRef` to the _PersistentVolumeClaim_.
.. End and not requeue
. Otherwise (there isn't a available _KukuVolumeItem_):
.. Check if the _KukuVolume_ has reached the maximum number of _volumeetos_ and abort and requeue if so.
.. Create a new _KukuVolumeItem_ setting its `spec.persistentVolumeClaimRef` to the name of the _PersistentVolumeClaim_. It no sets the `status.phase` to any value.
.. End anot requeue

Events generated by this controller:

* Related to _KukuVolumeItems_ objects:
** When a _KukuVolumeItem_ is created (ot the operation fails)
** When its `spec.persistentVolumeClaimRef` is set or unset (or the opration fails)
* Related to _KukuVolume_ objects:
** When a _KukuVolumeItem_ cannot be created because the maximum items has been reached.
* Related to _PersistentVolumeClaim_ objects:
** When the `kumori.systems/kukuvolume-pvc-finalizer` is added or removed (or this operation fails).
* Related to _PersistentVolume_ objects:
** When their `spec.claimRef` is set or removed (or this operation fails)

Notes about this controller:

* It creates _KukuVolumeItem_ objects and updates their specification but never their status. The `spec.persistentVolumeClaimRef` is set with the name of the _PersistentVolumeClaim_ which fired the _KukuVolumeItem_ creation.
* It sets the `spec.claimRef` for _PersistentVolume_ objects assigned to _KukuVolumeItem_ objects but only when the _KukuVolumeItem_ was available without a _PersistentVolumeClaim_ already assigned.
* It registers the `kumori.systems/kukuvolume-pvc-finalizer` to clean references to a _PersistentVolumeClaim_ when it is removed.

==== KukuVolumeItem PVC Controller

This controller runs the following reconciliation algorithm:

. If the temporary _PersistentVolumeClaim_ `status.phase` is not `Bound`, ends without requeuing (it has not a _PersistentVolume_ assigned yet)
. Gets the _KukuVolumeItem_ named as the `kumori.systems/kukuvolumeitem` label value and sets its `spec.persistentVolume` to the content of `spec.volumeName` in the _PersistentVolumeClaim_.

Events generated by this controller:

* Related to _KukuVolumeItem_ objects:
** When its `spec.persistentVolume` is set or unset (or the operation fails)

Notes about this controller:

* It only changes _KukuVolumeItem_ objects spec.

=== KukuVolumeItem Controller

This controller will reconcile a _KukuVolumeItem_ object if it is created, updated or modified. It runs the following reconciliation algorithm:

. If the _KukuVolumeItem_ deletion timestamp is set:
.. If it has a `spec.persistentVolume` set, delete the related _PersistentVolume_ by setting its `spec.PersistentVolumeReclaimPolicy` to `Delete`. This will trigger the _PersistentVolume_ by Kubernetes controllers. It also unsets the `spec.persistentVolume` in the _KukuVolumeItem_, just in case.
.. Remove the `kumori.systems/kukuvolume-kvi-finalizer` finalizer from the _KukuVolumeItem_.
.. End and not requeues.
. Get the _KukuVolume_ of the _KukuVolumeItem_ and updates the _KukuVolumeItem_ `status.items`.
. If the `status.phase` is not set:
.. Add the `kumori.systems/kukuvolume-kvi-finalizer` finalizer to the _KukuVolumeItem_
.. If a temporary _PersistentVolumeClaim_ has not been previously created for this _KukuVolumeItem_
... Create a temporary _PersistentVolumeClaim_. This object is used to fire the creation of the _PeristentVolume_ backing the _KukuVolumeItem_. The _PersistentVolumeClaim_ will be created with the following specification:
.... Set the _KukuVolumeItem_ as an owner reference, just in case.
.... Its name is `<KUKU_VOLUME_ITEM_NAME>-pvc`, being `<KUKU_VOLUME_ITEM_NAME>` the name of the _KukuVolumeItem_ object.
.... Set the `kumori.systems/kukuvolumeitem` label with the name of the _KukuVolumeItem_. This will identify this _PersistentVolumeClaim_ as temporary to create a _PersistentVolume_ fot this _KukuVolumeItem_.
.... Set the `spec.storageClass` to the storage class assigned to the type set in `spec.type` of the _KukuVolume_.
.... Set the `spec.accessModes` to `ReadWriteOnce`.
.... Set `spec.resources` to the value in `spec.size` of the _KukuVolume_
.. Change the `spec.phase` to `Creating`.
.. End and not requeue
. If the `status.phase` is `Creating`:
.. If it not has the `spec.persistentVolume` set and there isn't a temporary _PersistentVolumeClaim_ being processed, unset the _KukuVolumeItem_ `spec.phase`. Ends and requeues (the _KukuVolumeItem_ is not in the correct phase)
.. Remove the temporary _PersistentVolumeClaim_ objects assigned to this _KukuVolumeItem_ (if any).
.. Get the _PersistentVolume_ object named as `spec.persistentVolume` of the _KukuVolumeItem_ and set its  `spec.claimRef` using the name in `spec.persistentVolumeClaimRef` of the _KukuVolumeItem_.
.. Change the `spec.phase` to `Available`.
.. End and not requeue.
. If the `status.phase` is `Available`:
.. If the `spec.persistentVolumeClaimRef` is not set, end without requeuing (the _KukuVolumeItem_ is not assigned to any _PersistentVolumeClaim).
.. Change the `spec.phase` to `Binding`
.. End and not requeue
. If the `status.phase` is `Binding`:
.. If the `spec.persistentVolumeClaimRef` is not set, change `spec.phase` to `Available`. End and not requeue (the _PersistentVolumeClaim_ originally assigned to this _KukuVolumeItem_ has been deleted)
.. If the _PersistentVolumeClaim_ assigned to this _KukuVolumeItem_ is not in `Bound` phase yet, end and requeue (the binding process is already in process).
. If the `status.phase` is `Bound`:
.. If the `spec.persistentVolumeClaimRef` is not set, change `spec.phase` to `Available`. The _PersistentVolumeClaim_ originally assigned to this _KukuVolumeItem_ has been deleted.
.. End and not requeue

Events generated by this controller:

* Related to _KukuVolumeItem_ objects:
** When the `kumori.systems/kukuvolume-kvi-finalizer` is added or removed (or the operation fails)
** When the temporary _PersistentVolumeClaim_ is created or removed (or the operation fails)
** When the `status.phase` is changed (or the operation fails)
* Related to _PersistentVolume_ objects:
** When its `spec.claimRef` is updated (or the operation fails)
** When they are deleted (or the operation fails)

Notes about this controller:

* Updates the _KukuVolumeItem_ status but not the specification.
* Updates the _KukuVolume_ objects status (updates the number of existing items).
* Creates and removes the _PersistentVolume_ objects assigned to _KukuVolumeItem_ objects. Uses temporary _PersistentVolumeClaim_ for this end.
* Updates the _PersistentVolume_ objects `spec.claimRef` but only the first time, when the _KukuVolumeItem_ is created.

== License

Copyright 2022 Kumori Systems S.L.

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

https://joinup.ec.europa.eu/software/page/eupl

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and limitations under the Licence.